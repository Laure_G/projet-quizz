# Description de mon projet Quizz

Dans le cadre de ce projet, j'ai réalisé un quizz sur le thème de l'art et de la culture.

J'ai commencé par travailler sur les wiframes et maquettes à l'aide de [Figma](https://www.figma.com/file/7rVE78qlAXpTUC6vEWN6zf/Projet-Quizz?node-id=0%3A1&t=9FVnBbgNuDgfMImS-1) 


Le projet se compose de cinq parties: L'accueil, les consignes, les thèmes, le quizz et les résultats.

La structure de base est énoncée dans un fichier HTML et la mise à jour de contenu dynamique se fait via le script grâce à un fichier TypeScript lié. 
Le style est pris en charge par un fichier CSS lié et l'ensemble du site a été pensé en responsive à l'aide de bootstraps et des media queries pour pouvoir jouer aisément sur n'importe quel support : desktop, tablette ou mobile.
Ce quizz a été conçu pour que toute l'information soit immédiatement visible sur l'écran avec l'idée que l'interface utilisateur s'adapte à la taille de l'écran pour éviter trop de scroll pendant une partie.

Pour l'ensemble des parties devant apparaitre et disparaitre, une classe nommée "caché" avec un "display: none" a été créée et appelée avec des "ClassList.add" ou enlevée avec des "ClassList.remove".

La partie accueil comporte un bouton d'accès aux thèmes et un bouton d'accès aux consignes (selon si la personne joue pour la première fois ou non). Le CSS et bootstrap ont été utilisés pour centrer les boutons horizontalement et verticalement.

Pour la partie consignes, on a créé un tableau de consignes dans TypeScript dont les éléments on été intégrés dans une liste ordonnée insérée dans le HTML. Pour le style, on a ajouté des span, pour mettre en exergue certains éléments importants. Puis on a ajouté deux boutons avec des AddEventListener, l'un pour revenir à l'accueil et l'autre pour se diriger vers le choix de thème.

La partie thèmes, se compose de quatre boutons reliés à différents tableaux d'objets selon leur thématique. Ils permettent de lancer la partie quizz avec l'affichage des questions, leurs options de réponses, et de pouvoir comparer la réponse sélectionnée à la bonne réponse à trouver.

Pour la partie quizz, on l'a subdivisé en trois parties: un header, un container principal et un footer.
Dans le header on a inséré le titre du quizz à gauche en fonction du thème choisi et un chronomètre de 15 secondes à droite avec en dessous une barre de progression pour visualiser le temps qui s'écoule. On y a ajouté une condition qui joue sur les styles pour alerter le joueur lorsqu'il ne lui reste plus beaucoup de temps.
Dans le container principal, grâce au tableau d'objets sélectionné précédemment, on affiche une image, un intitulé de question et des options de réponses. Et à l'aide d' AddEventListener pour chaque option de réponses dans lesquels on intègre des conditions, on va déterminer l'ajout d'éléments de style et l'attribution de points en fonction de si la réponse est juste ou fausse. On va également lorsqu'on clique, stopper le chrono, rendre impossible toute autre réponse, afficher la correction et faire apparaitre le bouton suivant dans le footer.
Dans le footer, l'affichage de la question à laquelle on se trouve a été intégré à gauche, et un bouton suivant à droite pour pouvoir changer de question (ou afficher la partie résultat si l'on se trouve à la dernière question). Ce bouton n'apparaît qu'une fois que l'utilisateur a sélectionné une réponse ou que le temps de la question est écoulé.

Pour la partie résultat, on affiche le nombre de points obtenus en utilisant la concaténation pour afficher le résultat dans une phrase que l'on fait apparaitre dans le HTML. Et j'ai également mis en place un système de condition pour faire apparaitre un gif différent et une phrase différente en fonction du résultat de l'utilisateur.
Et enfin deux boutons permettent soit de revenir au lancement du quizz pour choisir un autre thème, soit de retenter à nouveau le même quizz.


## Aperçus des wireframes et maquettes

Wireframes et Maquettes sur Figma
![<alt>](</public/img/figma.png>)

Maquettes desktop
![<alt>](</public/img/maquettesQuizz.png>)

Maquettes mobile
![<alt>](</public/img/mobileMaquettes.png>)


## Consignes du projet

Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix (en gardant toujours en tête que
cette application pourrait être consultée par un·e employeur·se).

Fonctionnalités obligatoires:

- Affichage des questions et choix de la réponse
- Enchaînement de plusieurs questions (sans changement de page)
- Décompte des points et affichage du score (en temps réel, et/ou à la fin)
- Responsive

Ne pas hésiter à rajouter d'autres fonctionnalités une fois celles-ci implémentées (timer, plusieurs réponses possibles, réponse en champ de texte, illustrations pour certaines questions, etc.)

Compétences à mobilisées:
Dans l'idéal, vous devrez utiliser des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events). Essayer de maintenir votre code DRY.

Réalisation:
Commencer par trouver le thème de votre quizz et une liste de questions pour celui ci.
Ensuite réaliser une ou plusieurs maquettes fonctionnelles de ce à quoi ressemblera l'application.
Vous devrez rendre un projet gitlab avec un README présentant le projet et les maquettes (et à terme pourquoi pas
une explication de votre méthodologie pour le code).
Commentez votre code avec au moins la JS doc de vos fonctions.

## Les liens concernant le projet Portfolio

- [ ] [Figma](https://www.figma.com/file/7rVE78qlAXpTUC6vEWN6zf/Projet-Quizz?node-id=0%3A1&t=9FVnBbgNuDgfMImS-1) 
- [ ] [Gitlab](https://gitlab.com/Laure_G/projet-quizz/)
- [ ] [Netlify](https://quizz-culture.netlify.app/)

## Les outils utilisés

- HTML pour la structure, 
- CSS pour le style, 
- TypeScript pour l'interactivité et la mise à jour de contenu dynamique, 
- Bootstrap et les Media Queries pour le responsive, 
- Google Fonts Apis pour la police d'écriture, 
- Figma pour réaliser les wireframes et maquettes, 
- Visual Studio Code 
- Gitlab.

## Test et Mise en ligne

Le HML/ CSS de ce projet a été testé avec W3C puis a été mis en ligne en utilisant Netlify pour être testé par mes camarades et mes proches.


## Statut du projet

Ce projet pourra être amené à évoluer dans le temps, au fur et à mesure de mon parcours et de mon apprentissage.
