// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"aG0yL":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = null;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "38f98c549750828e";
"use strict";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE, chrome, browser, globalThis, __parcel__import__, __parcel__importScripts__, ServiceWorkerGlobalScope */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: mixed;
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
interface ExtensionContext {
  runtime: {|
    reload(): void,
    getURL(url: string): string;
    getManifest(): {manifest_version: number, ...};
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
declare var chrome: ExtensionContext;
declare var browser: ExtensionContext;
declare var __parcel__import__: (string) => Promise<void>;
declare var __parcel__importScripts__: (string) => Promise<void>;
declare var globalThis: typeof self;
declare var ServiceWorkerGlobalScope: Object;
*/ var OVERLAY_ID = "__parcel__error__overlay__";
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData,
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function(fn) {
            this._acceptCallbacks.push(fn || function() {});
        },
        dispose: function(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData = undefined;
}
module.bundle.Module = Module;
var checkedAssets, acceptedAssets, assetsToAccept /*: Array<[ParcelRequire, string]> */ ;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf("http") === 0 ? location.hostname : "localhost");
}
function getPort() {
    return HMR_PORT || location.port;
} // eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== "undefined") {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == "https:" && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? "wss" : "ws";
    var ws = new WebSocket(protocol + "://" + hostname + (port ? ":" + port : "") + "/"); // Web extension context
    var extCtx = typeof chrome === "undefined" ? typeof browser === "undefined" ? null : browser : chrome; // Safari doesn't support sourceURL in error stacks.
    // eval may also be disabled via CSP, so do a quick check.
    var supportsSourceURL = false;
    try {
        (0, eval)('throw new Error("test"); //# sourceURL=test.js');
    } catch (err) {
        supportsSourceURL = err.stack.includes("test.js");
    } // $FlowFixMe
    ws.onmessage = async function(event) {
        checkedAssets = {} /*: {|[string]: boolean|} */ ;
        acceptedAssets = {} /*: {|[string]: boolean|} */ ;
        assetsToAccept = [];
        var data = JSON.parse(event.data);
        if (data.type === "update") {
            // Remove error overlay if there is one
            if (typeof document !== "undefined") removeErrorOverlay();
            let assets = data.assets.filter((asset)=>asset.envHash === HMR_ENV_HASH); // Handle HMR Update
            let handled = assets.every((asset)=>{
                return asset.type === "css" || asset.type === "js" && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear(); // Dispatch custom event so other runtimes (e.g React Refresh) are aware.
                if (typeof window !== "undefined" && typeof CustomEvent !== "undefined") window.dispatchEvent(new CustomEvent("parcelhmraccept"));
                await hmrApplyUpdates(assets);
                for(var i = 0; i < assetsToAccept.length; i++){
                    var id = assetsToAccept[i][1];
                    if (!acceptedAssets[id]) hmrAcceptRun(assetsToAccept[i][0], id);
                }
            } else fullReload();
        }
        if (data.type === "error") {
            // Log parcel errors to console
            for (let ansiDiagnostic of data.diagnostics.ansi){
                let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                console.error("\uD83D\uDEA8 [parcel]: " + ansiDiagnostic.message + "\n" + stack + "\n\n" + ansiDiagnostic.hints.join("\n"));
            }
            if (typeof document !== "undefined") {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html); // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        console.error(e.message);
    };
    ws.onclose = function() {
        console.warn("[parcel] \uD83D\uDEA8 Connection to the HMR server was lost");
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log("[parcel] ✨ Error resolved");
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement("div");
    overlay.id = OVERLAY_ID;
    let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    for (let diagnostic of diagnostics){
        let stack = diagnostic.frames.length ? diagnostic.frames.reduce((p, frame)=>{
            return `${p}
<a href="/__parcel_launch_editor?file=${encodeURIComponent(frame.location)}" style="text-decoration: underline; color: #888" onclick="fetch(this.href); return false">${frame.location}</a>
${frame.code}`;
        }, "") : diagnostic.stack;
        errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          🚨 ${diagnostic.message}
        </div>
        <pre>${stack}</pre>
        <div>
          ${diagnostic.hints.map((hint)=>"<div>\uD83D\uDCA1 " + hint + "</div>").join("")}
        </div>
        ${diagnostic.documentation ? `<div>📝 <a style="color: violet" href="${diagnostic.documentation}" target="_blank">Learn more</a></div>` : ""}
      </div>
    `;
    }
    errorHTML += "</div>";
    overlay.innerHTML = errorHTML;
    return overlay;
}
function fullReload() {
    if ("reload" in location) location.reload();
    else if (extCtx && extCtx.runtime && extCtx.runtime.reload) extCtx.runtime.reload();
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute("href", link.getAttribute("href").split("?")[0] + "?" + Date.now()); // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href = links[i].getAttribute("href");
            var hostname = getHostname();
            var servedFromHMRServer = hostname === "localhost" ? new RegExp("^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):" + getPort()).test(href) : href.indexOf(hostname + ":" + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrDownload(asset) {
    if (asset.type === "js") {
        if (typeof document !== "undefined") {
            let script = document.createElement("script");
            script.src = asset.url + "?t=" + Date.now();
            if (asset.outputFormat === "esmodule") script.type = "module";
            return new Promise((resolve, reject)=>{
                var _document$head;
                script.onload = ()=>resolve(script);
                script.onerror = reject;
                (_document$head = document.head) === null || _document$head === void 0 || _document$head.appendChild(script);
            });
        } else if (typeof importScripts === "function") {
            // Worker scripts
            if (asset.outputFormat === "esmodule") return import(asset.url + "?t=" + Date.now());
            else return new Promise((resolve, reject)=>{
                try {
                    importScripts(asset.url + "?t=" + Date.now());
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        }
    }
}
async function hmrApplyUpdates(assets) {
    global.parcelHotUpdate = Object.create(null);
    let scriptsToRemove;
    try {
        // If sourceURL comments aren't supported in eval, we need to load
        // the update from the dev server over HTTP so that stack traces
        // are correct in errors/logs. This is much slower than eval, so
        // we only do it if needed (currently just Safari).
        // https://bugs.webkit.org/show_bug.cgi?id=137297
        // This path is also taken if a CSP disallows eval.
        if (!supportsSourceURL) {
            let promises = assets.map((asset)=>{
                var _hmrDownload;
                return (_hmrDownload = hmrDownload(asset)) === null || _hmrDownload === void 0 ? void 0 : _hmrDownload.catch((err)=>{
                    // Web extension bugfix for Chromium
                    // https://bugs.chromium.org/p/chromium/issues/detail?id=1255412#c12
                    if (extCtx && extCtx.runtime && extCtx.runtime.getManifest().manifest_version == 3) {
                        if (typeof ServiceWorkerGlobalScope != "undefined" && global instanceof ServiceWorkerGlobalScope) {
                            extCtx.runtime.reload();
                            return;
                        }
                        asset.url = extCtx.runtime.getURL("/__parcel_hmr_proxy__?url=" + encodeURIComponent(asset.url + "?t=" + Date.now()));
                        return hmrDownload(asset);
                    }
                    throw err;
                });
            });
            scriptsToRemove = await Promise.all(promises);
        }
        assets.forEach(function(asset) {
            hmrApply(module.bundle.root, asset);
        });
    } finally{
        delete global.parcelHotUpdate;
        if (scriptsToRemove) scriptsToRemove.forEach((script)=>{
            if (script) {
                var _document$head2;
                (_document$head2 = document.head) === null || _document$head2 === void 0 || _document$head2.removeChild(script);
            }
        });
    }
}
function hmrApply(bundle, asset) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === "css") reloadCSS();
    else if (asset.type === "js") {
        let deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                let oldDeps = modules[asset.id][1];
                for(let dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    let id = oldDeps[dep];
                    let parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            if (supportsSourceURL) // Global eval. We would use `new Function` here but browser
            // support for source maps is better with eval.
            (0, eval)(asset.output);
             // $FlowFixMe
            let fn = global.parcelHotUpdate[asset.id];
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id) {
    let modules = bundle.modules;
    if (!modules) return;
    if (modules[id]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        let deps = modules[id][1];
        let orphans = [];
        for(let dep in deps){
            let parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        } // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id];
        delete bundle.cache[id]; // Now delete the orphans.
        orphans.forEach((id)=>{
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id);
}
function hmrAcceptCheck(bundle, id, depsByBundle) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
     // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    let parents = getParents(module.bundle.root, id);
    let accepted = false;
    while(parents.length > 0){
        let v = parents.shift();
        let a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            let p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push(...p);
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle, id, depsByBundle) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToAccept.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) return true;
}
function hmrAcceptRun(bundle, id) {
    var cached = bundle.cache[id];
    bundle.hotData = {};
    if (cached && cached.hot) cached.hot.data = bundle.hotData;
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData);
    });
    delete bundle.cache[id];
    bundle(id);
    cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) // $FlowFixMe[method-unbinding]
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
    });
    acceptedAssets[id] = true;
}

},{}],"5Deyb":[function(require,module,exports) {
console.log("hello");
/* Les variables*/ const start_btn = document.querySelector(".start_btn button");
const Infos = document.querySelector(".Infos");
const quitter = document.querySelector(".quitter");
const continuer = document.querySelector(".continuer");
const ContainerQuizz = document.querySelector(".ContainerQuizz");
const listeChoix = document.querySelector(".listeChoix");
const TexteQuestion = document.querySelector(".TexteQuestion");
const suivant = document.querySelector(".suivant");
const ImageQuestion = document.querySelector(".ImageQuestion");
const evoChrono = document.querySelector("header .evoChrono");
const timeText = document.querySelector(".chrono .time_left_txt");
const timeCount = document.querySelector(".chrono .timer_sec");
/* Le lancement du Quizz avec affichage des consignes*/ start_btn?.addEventListener("click", ()=>{
    if (Infos) {
        Infos.classList.remove("cach\xe9");
        start_btn.classList.add("cach\xe9");
    }
});
/* Quitter les consignes*/ quitter?.addEventListener("click", ()=>{
    if (Infos && start_btn) {
        Infos.classList.add("cach\xe9");
        start_btn.classList.remove("cach\xe9");
    }
});
/* Commencer le jeu */ continuer?.addEventListener("click", ()=>{
    if (Infos && ContainerQuizz) {
        Infos.classList.add("cach\xe9");
        ContainerQuizz.classList.remove("cach\xe9");
        AfficherLaQuestion(0);
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0);
    }
});
let index = 0;
/*Fonction pour afficher les questions et leurs options*/ function AfficherLaQuestion(index) {
    let imageAffiche = '<div class="image"><span>' + questions[index].image + "</span></div>";
    let questionAffiche = "<span>" + questions[index].numb + ". " + questions[index].question + "</span>";
    let optionsAffiche = '<div class="option"><span>' + questions[index].options[0] + "</span></div>" + '<div class="option"><span>' + questions[index].options[1] + "</span></div>" + '<div class="option"><span>' + questions[index].options[2] + "</span></div>" + '<div class="option"><span>' + questions[index].options[3] + "</span></div>";
    if (TexteQuestion && listeChoix && ImageQuestion) {
        TexteQuestion.innerHTML = questionAffiche;
        listeChoix.innerHTML = optionsAffiche;
        ImageQuestion.innerHTML = imageAffiche;
    }
    /* Donner un evenement clicable sur chaque option avec des conséquences différentes selon si c'est correct ou non */ if (listeChoix) {
        const options = listeChoix.querySelectorAll(".option");
        let i;
        for (const itemOption of options)itemOption.addEventListener("click", ()=>{
            if (itemOption.textContent == questions[index].answer) {
                ScoreRéalisé += 1;
                itemOption.classList.add("correct");
                console.log("Bonne r\xe9ponse, tu as " + ScoreRéalisé + " points");
            }
            if (itemOption.textContent != questions[index].answer) {
                itemOption.classList.add("incorrect");
                console.log("Mauvaise r\xe9ponse, dommage!");
                /*Afficher qu'elle était la bonne réponse si jamais on se trompe*/ for(i = 0; i < options.length; i++)if (listeChoix.children[i].textContent == questions[index].answer) {
                    listeChoix.children[i].setAttribute("class", "option correct");
                    console.log("Voici ce qu'il fallait r\xe9pondre: " + options[i].textContent);
                }
            }
            /*Empecher de cliquer sur une autre question une fois que l'on a cliquer sur sa réponse*/ for(i = 0; i < options.length; i++)if (listeChoix) listeChoix.children[i].classList.add("disabled");
            /*Ajouter le bouton suivant pour pouvoir changer de question*/ if (suivant) suivant.classList.add("show");
        });
    }
}
let que_numb = 1;
let ScoreRéalisé = 0;
let counter;
let counterLine;
let widthValue = 0;
/*Cliquer sur le bouton suivant pour changer de question*/ suivant?.addEventListener("click", ()=>{
    if (index < questions.length - 1) {
        index++;
        que_numb++;
        AfficherLaQuestion(index);
        AvancéeDesQuestions(que_numb);
        clearInterval(counter);
        clearInterval(counterLine);
        LancerChrono(timeValue);
        Evolutionchrono(widthValue);
        if (timeText) timeText.textContent = "Chrono";
        suivant.classList.remove("show");
    } else {
        clearInterval(counter);
        clearInterval(counterLine);
        AfficherResultat();
    }
});
const bottom_ques_counter = document.querySelector("footer .TotalPoints");
/*Afficher l'avancée du quizz, pour indiquer à quelle question on se trouve*/ function AvancéeDesQuestions(index) {
    let totalQueCounTag = "<span><p>Question " + index + "</p> / <p>" + questions.length + "</p></span>";
    if (bottom_ques_counter) bottom_ques_counter.innerHTML = totalQueCounTag;
}
let timeValue = 15;
/*Déclenchement du chrono*/ function LancerChrono(time) {
    counter = setInterval(chrono, 1000);
    function chrono() {
        if (timeCount) {
            timeCount.textContent = time;
            time--;
            if (time < 9) {
                let addZero = timeCount.textContent;
                timeCount.textContent = "0" + addZero;
            }
            if (time < 0) {
                clearInterval(counter);
                if (timeText) timeText.textContent = "Termin\xe9";
                const ToutesOptions = listeChoix?.children.length;
                let correcAns = questions[index].answer;
                if (ToutesOptions) {
                    let i;
                    for(i = 0; i < ToutesOptions; i++)if (listeChoix?.children[i].textContent == correcAns) {
                        listeChoix?.children[i].setAttribute("class", "option correct");
                        console.log("Temps \xe9coul\xe9, voici la bonne r\xe9ponse.");
                    }
                    for(i = 0; i < ToutesOptions; i++)listeChoix?.children[i].classList.add("disabled");
                    suivant?.classList.add("show");
                }
            }
        }
    }
}
const ContainerResultat = document.querySelector(".ContainerResultat");
/*Afficher le container contenant le résultat*/ function AfficherResultat() {
    ContainerQuizz?.classList.add("cach\xe9");
    ContainerResultat?.classList.remove("cach\xe9");
    const scoreText = ContainerResultat?.querySelector(".MessageResultat");
    /* Afficher un message différent en fonction du score */ if (ScoreRéalisé > 3) {
        let scoreTag = "<span> F\xe9licitations ! Tu as " + ScoreRéalisé + " points sur " + questions.length + ".</span>";
        if (scoreText) scoreText.innerHTML = scoreTag;
    }
    if (ScoreRéalisé < 3) {
        let scoreTag1 = "<span> C'est bien ! Tu as " + ScoreRéalisé + " points sur " + questions.length + ".</span>";
        if (scoreText) scoreText.innerHTML = scoreTag1;
    }
    if (ScoreRéalisé < 2) {
        let scoreTag2 = "<span> D\xe9sol\xe9, ton score est seulement de " + ScoreRéalisé + " point sur " + questions.length + ".</span>";
        if (scoreText) scoreText.innerHTML = scoreTag2;
    }
}
/*Représenter le temps restant avec une barre de progression*/ function Evolutionchrono(time) {
    counterLine = setInterval(chrono, 160);
    function chrono() {
        time += 1;
        if (evoChrono) evoChrono.style.width = time + "%";
        if (time > 50) {
            if (evoChrono) evoChrono.style.backgroundColor = "orange";
        }
        if (time > 80) {
            if (evoChrono) evoChrono.style.backgroundColor = "red";
        }
        if (time > 101) clearInterval(counterLine);
    }
}
/* Si on clique sur le bouton Rejouer*/ const rejouer = ContainerResultat?.querySelector(".buttons .continuer");
rejouer?.addEventListener("click", ()=>{
    ContainerQuizz?.classList.remove("cach\xe9");
    ContainerResultat?.classList.add("cach\xe9");
    timeValue = 15;
    index = 0;
    que_numb = 1;
    ScoreRéalisé = 0;
    widthValue = 0;
    AfficherLaQuestion(index);
    AvancéeDesQuestions(que_numb);
    clearInterval(counter);
    clearInterval(counterLine);
    LancerChrono(timeValue);
    Evolutionchrono(widthValue);
    if (timeText) timeText.textContent = "Temps";
    suivant?.classList.remove("show");
});
/* Si on clique sur le bouton pour quitter le jeu*/ const stop = ContainerResultat?.querySelector(".buttons .arret");
stop?.addEventListener("click", ()=>{
    window.location.reload();
});
/* Tableau des différentes questions avec leurs images et leurs réponses */ let questions = [
    {
        image: '<img src="https://www.boutiquesdemusees.fr/uploads/photos/474/11220_xl.jpg" alt="illustration" style="width:70%;">',
        numb: 1,
        question: "Qui a r\xe9alis\xe9 Falling Water?",
        answer: "Franck Loyd Wright",
        options: [
            "Kawamata",
            "Jean nouvel",
            "Franck Loyd Wright",
            "Baltard"
        ]
    },
    {
        image: '<img src="https://www.alhuilesurtoile.com/im/articles/Warhol_MarilynMonroe_x9.jpg" alt="illustration" style="width:70%;">',
        numb: 2,
        question: "Qui a peint Marilyn Monroe?",
        answer: "Andy Warhol",
        options: [
            "Matisse",
            "Van Gogh",
            "Magritte",
            "Andy Warhol"
        ]
    },
    {
        image: '<img src="https://i0.wp.com/art-icle.fr/wp-content/uploads/2019/09/Giorgio-De-Chirico-copie.jpg?w=643&ssl=1" alt="illustration" style="width:70%;">',
        numb: 3,
        question: "Qui a peint ce portrait de Guillaume Appolinaire?",
        answer: "Giorgio De Chirico",
        options: [
            "Giorgio De Chirico",
            "Dali",
            "Picasso",
            "Renoir"
        ]
    },
    {
        image: '<img src="https://www.office-et-culture.fr/uploads/eff9fa930c4c43e3431763d6e8827341d1fb175e.jpg" alt="illustration" style="width:70%;">',
        numb: 4,
        question: "O\xf9 se trouve cette biblioth\xe8que con\xe7ue par le cabinet d'architecture X + Living?",
        answer: "En Chine",
        options: [
            "En Allemagne",
            "En Angleterre",
            "En Chine",
            "Aux Etats-Unis"
        ]
    }
];

},{}]},["aG0yL","5Deyb"], "5Deyb", "parcelRequire529b")

//# sourceMappingURL=architecture.9750828e.js.map
