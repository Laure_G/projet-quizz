// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"eEc30":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = null;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "6f8beed154632f9a";
"use strict";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE, chrome, browser, globalThis, __parcel__import__, __parcel__importScripts__, ServiceWorkerGlobalScope */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: mixed;
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
interface ExtensionContext {
  runtime: {|
    reload(): void,
    getURL(url: string): string;
    getManifest(): {manifest_version: number, ...};
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
declare var chrome: ExtensionContext;
declare var browser: ExtensionContext;
declare var __parcel__import__: (string) => Promise<void>;
declare var __parcel__importScripts__: (string) => Promise<void>;
declare var globalThis: typeof self;
declare var ServiceWorkerGlobalScope: Object;
*/ var OVERLAY_ID = "__parcel__error__overlay__";
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData,
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function(fn) {
            this._acceptCallbacks.push(fn || function() {});
        },
        dispose: function(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData = undefined;
}
module.bundle.Module = Module;
var checkedAssets, acceptedAssets, assetsToAccept /*: Array<[ParcelRequire, string]> */ ;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf("http") === 0 ? location.hostname : "localhost");
}
function getPort() {
    return HMR_PORT || location.port;
} // eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== "undefined") {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == "https:" && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? "wss" : "ws";
    var ws = new WebSocket(protocol + "://" + hostname + (port ? ":" + port : "") + "/"); // Web extension context
    var extCtx = typeof chrome === "undefined" ? typeof browser === "undefined" ? null : browser : chrome; // Safari doesn't support sourceURL in error stacks.
    // eval may also be disabled via CSP, so do a quick check.
    var supportsSourceURL = false;
    try {
        (0, eval)('throw new Error("test"); //# sourceURL=test.js');
    } catch (err) {
        supportsSourceURL = err.stack.includes("test.js");
    } // $FlowFixMe
    ws.onmessage = async function(event) {
        checkedAssets = {} /*: {|[string]: boolean|} */ ;
        acceptedAssets = {} /*: {|[string]: boolean|} */ ;
        assetsToAccept = [];
        var data = JSON.parse(event.data);
        if (data.type === "update") {
            // Remove error overlay if there is one
            if (typeof document !== "undefined") removeErrorOverlay();
            let assets = data.assets.filter((asset)=>asset.envHash === HMR_ENV_HASH); // Handle HMR Update
            let handled = assets.every((asset)=>{
                return asset.type === "css" || asset.type === "js" && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear(); // Dispatch custom event so other runtimes (e.g React Refresh) are aware.
                if (typeof window !== "undefined" && typeof CustomEvent !== "undefined") window.dispatchEvent(new CustomEvent("parcelhmraccept"));
                await hmrApplyUpdates(assets);
                for(var i = 0; i < assetsToAccept.length; i++){
                    var id = assetsToAccept[i][1];
                    if (!acceptedAssets[id]) hmrAcceptRun(assetsToAccept[i][0], id);
                }
            } else fullReload();
        }
        if (data.type === "error") {
            // Log parcel errors to console
            for (let ansiDiagnostic of data.diagnostics.ansi){
                let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                console.error("\uD83D\uDEA8 [parcel]: " + ansiDiagnostic.message + "\n" + stack + "\n\n" + ansiDiagnostic.hints.join("\n"));
            }
            if (typeof document !== "undefined") {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html); // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        console.error(e.message);
    };
    ws.onclose = function() {
        console.warn("[parcel] \uD83D\uDEA8 Connection to the HMR server was lost");
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log("[parcel] ✨ Error resolved");
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement("div");
    overlay.id = OVERLAY_ID;
    let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    for (let diagnostic of diagnostics){
        let stack = diagnostic.frames.length ? diagnostic.frames.reduce((p, frame)=>{
            return `${p}
<a href="/__parcel_launch_editor?file=${encodeURIComponent(frame.location)}" style="text-decoration: underline; color: #888" onclick="fetch(this.href); return false">${frame.location}</a>
${frame.code}`;
        }, "") : diagnostic.stack;
        errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          🚨 ${diagnostic.message}
        </div>
        <pre>${stack}</pre>
        <div>
          ${diagnostic.hints.map((hint)=>"<div>\uD83D\uDCA1 " + hint + "</div>").join("")}
        </div>
        ${diagnostic.documentation ? `<div>📝 <a style="color: violet" href="${diagnostic.documentation}" target="_blank">Learn more</a></div>` : ""}
      </div>
    `;
    }
    errorHTML += "</div>";
    overlay.innerHTML = errorHTML;
    return overlay;
}
function fullReload() {
    if ("reload" in location) location.reload();
    else if (extCtx && extCtx.runtime && extCtx.runtime.reload) extCtx.runtime.reload();
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute("href", link.getAttribute("href").split("?")[0] + "?" + Date.now()); // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href = links[i].getAttribute("href");
            var hostname = getHostname();
            var servedFromHMRServer = hostname === "localhost" ? new RegExp("^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):" + getPort()).test(href) : href.indexOf(hostname + ":" + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrDownload(asset) {
    if (asset.type === "js") {
        if (typeof document !== "undefined") {
            let script = document.createElement("script");
            script.src = asset.url + "?t=" + Date.now();
            if (asset.outputFormat === "esmodule") script.type = "module";
            return new Promise((resolve, reject)=>{
                var _document$head;
                script.onload = ()=>resolve(script);
                script.onerror = reject;
                (_document$head = document.head) === null || _document$head === void 0 || _document$head.appendChild(script);
            });
        } else if (typeof importScripts === "function") {
            // Worker scripts
            if (asset.outputFormat === "esmodule") return import(asset.url + "?t=" + Date.now());
            else return new Promise((resolve, reject)=>{
                try {
                    importScripts(asset.url + "?t=" + Date.now());
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        }
    }
}
async function hmrApplyUpdates(assets) {
    global.parcelHotUpdate = Object.create(null);
    let scriptsToRemove;
    try {
        // If sourceURL comments aren't supported in eval, we need to load
        // the update from the dev server over HTTP so that stack traces
        // are correct in errors/logs. This is much slower than eval, so
        // we only do it if needed (currently just Safari).
        // https://bugs.webkit.org/show_bug.cgi?id=137297
        // This path is also taken if a CSP disallows eval.
        if (!supportsSourceURL) {
            let promises = assets.map((asset)=>{
                var _hmrDownload;
                return (_hmrDownload = hmrDownload(asset)) === null || _hmrDownload === void 0 ? void 0 : _hmrDownload.catch((err)=>{
                    // Web extension bugfix for Chromium
                    // https://bugs.chromium.org/p/chromium/issues/detail?id=1255412#c12
                    if (extCtx && extCtx.runtime && extCtx.runtime.getManifest().manifest_version == 3) {
                        if (typeof ServiceWorkerGlobalScope != "undefined" && global instanceof ServiceWorkerGlobalScope) {
                            extCtx.runtime.reload();
                            return;
                        }
                        asset.url = extCtx.runtime.getURL("/__parcel_hmr_proxy__?url=" + encodeURIComponent(asset.url + "?t=" + Date.now()));
                        return hmrDownload(asset);
                    }
                    throw err;
                });
            });
            scriptsToRemove = await Promise.all(promises);
        }
        assets.forEach(function(asset) {
            hmrApply(module.bundle.root, asset);
        });
    } finally{
        delete global.parcelHotUpdate;
        if (scriptsToRemove) scriptsToRemove.forEach((script)=>{
            if (script) {
                var _document$head2;
                (_document$head2 = document.head) === null || _document$head2 === void 0 || _document$head2.removeChild(script);
            }
        });
    }
}
function hmrApply(bundle, asset) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === "css") reloadCSS();
    else if (asset.type === "js") {
        let deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                let oldDeps = modules[asset.id][1];
                for(let dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    let id = oldDeps[dep];
                    let parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            if (supportsSourceURL) // Global eval. We would use `new Function` here but browser
            // support for source maps is better with eval.
            (0, eval)(asset.output);
             // $FlowFixMe
            let fn = global.parcelHotUpdate[asset.id];
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id) {
    let modules = bundle.modules;
    if (!modules) return;
    if (modules[id]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        let deps = modules[id][1];
        let orphans = [];
        for(let dep in deps){
            let parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        } // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id];
        delete bundle.cache[id]; // Now delete the orphans.
        orphans.forEach((id)=>{
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id);
}
function hmrAcceptCheck(bundle, id, depsByBundle) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
     // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    let parents = getParents(module.bundle.root, id);
    let accepted = false;
    while(parents.length > 0){
        let v = parents.shift();
        let a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            let p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push(...p);
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle, id, depsByBundle) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToAccept.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) return true;
}
function hmrAcceptRun(bundle, id) {
    var cached = bundle.cache[id];
    bundle.hotData = {};
    if (cached && cached.hot) cached.hot.data = bundle.hotData;
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData);
    });
    delete bundle.cache[id];
    bundle(id);
    cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) // $FlowFixMe[method-unbinding]
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
    });
    acceptedAssets[id] = true;
}

},{}],"gg0zR":[function(require,module,exports) {
//Les variables
const start_btn = document.querySelector(".start_btn");
const start = document.querySelector(".start");
const consignes = document.querySelector(".consignes");
const ListeConsignes = [
    " Vous aurez un temps limit\xe9 de <span>15 secondes</span> pour r\xe9pondre \xe0 chaque question.",
    " Toute r\xe9ponse s\xe9lectionn\xe9e est <span>d\xe9finitive</span>, vous ne pourrez cliquer qu'une fois.",
    " Vous gagnerez <span>1 point </span>\xe0 chaque bonne r\xe9ponse."
];
const ListInfo = document.querySelector(".ListInfo");
const Infos = document.querySelector(".Infos");
const ContainerTheme = document.querySelector(".ContainerTheme");
const quitter = document.querySelector(".quitter");
const continuer = document.querySelector(".continuer");
const art = document.querySelector(".art");
const archi = document.querySelector(".architecture");
const jeux = document.querySelector(".jeux");
const cine = document.querySelector(".cine");
const ContainerQuizz = document.querySelector(".ContainerQuizz");
const QuizzTitre = document.querySelector(".QuizzTitre");
const listeChoix = document.querySelector(".listeChoix");
const TexteQuestion = document.querySelector(".TexteQuestion");
const suivant = document.querySelector(".suivant");
const ImageQuestion = document.querySelector(".ImageQuestion");
const evoChrono = document.querySelector("header .evoChrono");
const timeText = document.querySelector(".chrono .time_left_txt");
const timeCount = document.querySelector(".chrono .timer_sec");
const CaseChrono = document.querySelector(".chrono");
const compteurAvancéQuestion = document.querySelector("footer .TotalQuestion");
const ContainerResultat = document.querySelector(".ContainerResultat");
const MessageResultat = document.querySelector(".MessageResultat");
const imageResultat = document.querySelector(".imageResultat");
// L'addEventListener pour le lancement du Quizz avec l'affichage des thèmes.
start?.addEventListener("click", ()=>{
    if (start_btn && ContainerTheme) {
        ContainerTheme.classList.remove("cach\xe9");
        start_btn.classList.add("cach\xe9");
    }
});
//Création d'une boucle avec l'intégration du tableau de consignes dans le HTML sous forme de liste ordonnée.
let element;
for (element of ListeConsignes){
    let li = document.createElement("li");
    li.classList.add("info");
    ListInfo?.appendChild(li);
    li.innerHTML = element;
}
// L'addEventListener pour le lancement du Quizz avec affichage des consignes.
consignes?.addEventListener("click", ()=>{
    if (Infos && start_btn) {
        Infos.classList.remove("cach\xe9");
        start_btn.classList.add("cach\xe9");
    }
});
// L'addEventListener pour quitter les consignes
quitter?.addEventListener("click", ()=>{
    if (Infos && start_btn) {
        Infos.classList.add("cach\xe9");
        start_btn.classList.remove("cach\xe9");
    }
});
// L'addEventListener pour commencer le jeu 
continuer?.addEventListener("click", ()=>{
    if (Infos && ContainerTheme) {
        Infos.classList.add("cach\xe9");
        ContainerTheme.classList.remove("cach\xe9");
    }
});
// L'addEventListener pour choisir le thème Art 
art?.addEventListener("click", ()=>{
    if (ContainerTheme && ContainerQuizz) {
        ContainerTheme.classList.add("cach\xe9");
        ContainerQuizz.classList.remove("cach\xe9");
        AfficherLaQuestion(0);
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0);
        if (QuizzTitre) QuizzTitre.innerHTML = "Quizz Art";
    }
});
// L'addEventListener pour choisir le thème Architecture
archi?.addEventListener("click", ()=>{
    if (ContainerTheme && ContainerQuizz) {
        ContainerTheme.classList.add("cach\xe9");
        ContainerQuizz.classList.remove("cach\xe9");
        questions = questionsarchi;
        AfficherLaQuestion(0);
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0);
        if (QuizzTitre) QuizzTitre.innerHTML = "Quizz Architecture";
    }
});
// L'addEventListener pour choisir le thème Jeux Vidéos
jeux?.addEventListener("click", ()=>{
    if (ContainerTheme && ContainerQuizz) {
        ContainerTheme.classList.add("cach\xe9");
        ContainerQuizz.classList.remove("cach\xe9");
        questions = questionsjv;
        AfficherLaQuestion(0);
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0);
        if (QuizzTitre) QuizzTitre.innerHTML = "Quizz Jeux Vid\xe9os";
    }
});
// L'addEventListener pour choisir le thème Cinéma
cine?.addEventListener("click", ()=>{
    if (ContainerTheme && ContainerQuizz) {
        ContainerTheme.classList.add("cach\xe9");
        ContainerQuizz.classList.remove("cach\xe9");
        questions = questionscine;
        AfficherLaQuestion(0);
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0);
        if (QuizzTitre) QuizzTitre.innerHTML = "Quizz Cin\xe9ma";
    }
});
let index = 0;
/**
 * Fonction pour afficher les questions et leurs options de réponse dans laquelle on a intégré le addEventListener de chaque option et leurs conséquences si la réponse est juste ou non.
 * @param index l'index de la question que l'on souhaite afficher avec ses options de réponse*/ function AfficherLaQuestion(index) {
    let imageAffiche = '<div class="image"><span>' + questions[index].image + "</span></div>";
    let questionAffiche = "<span>" + questions[index].numb + ". " + questions[index].question + "</span>";
    let optionsAffiche = '<div class="option"><span>' + questions[index].options[0] + "</span></div>" + '<div class="option"><span>' + questions[index].options[1] + "</span></div>" + '<div class="option"><span>' + questions[index].options[2] + "</span></div>" + '<div class="option"><span>' + questions[index].options[3] + "</span></div>";
    if (TexteQuestion && listeChoix && ImageQuestion) {
        TexteQuestion.innerHTML = questionAffiche;
        listeChoix.innerHTML = optionsAffiche;
        ImageQuestion.innerHTML = imageAffiche;
    }
    // L'addEventListener pour donner un évenement clicable sur chaque option avec des conséquences différentes selon si c'est correct ou non
    if (listeChoix) {
        const options = listeChoix.querySelectorAll(".option");
        let i;
        //On boucle pour pouvoir créer l'évènement click sur chaque option
        for (const itemOption of options)itemOption.addEventListener("click", ()=>{
            //Arrêt du chrono
            clearInterval(counter);
            if (timeText) timeText.textContent = "Termin\xe9";
            clearInterval(counterLine);
            if (evoChrono && timeCount && CaseChrono) {
                evoChrono.style.backgroundColor = "#007bff";
                timeCount.style.backgroundColor = "#0b4681";
                timeCount.style.borderColor = "#0b4681";
                CaseChrono.style.backgroundColor = "#cce5ff";
                CaseChrono.style.color = "#004085";
                CaseChrono.style.borderColor = "#b8daff";
            }
            //Si la réponse est correcte
            if (itemOption.textContent == questions[index].answer) {
                ScoreRéalisé += 1;
                itemOption.classList.add("correct");
                console.log("Bonne r\xe9ponse, tu as " + ScoreRéalisé + " points");
            }
            //Si la réponse est fausse
            if (itemOption.textContent != questions[index].answer) {
                itemOption.classList.add("incorrect");
                console.log("Mauvaise r\xe9ponse, dommage!");
                //Afficher qu'elle était la bonne réponse si jamais on se trompe
                for(i = 0; i < options.length; i++)if (listeChoix.children[i].textContent == questions[index].answer) {
                    listeChoix.children[i].setAttribute("class", "option correct");
                    console.log("Voici ce qu'il fallait r\xe9pondre: " + options[i].textContent);
                }
            }
            //Empêcher de cliquer sur une autre question une fois que l'on a cliquer sur sa réponse
            for(i = 0; i < options.length; i++)if (listeChoix) listeChoix.children[i].classList.add("disabled");
            //Ajouter le bouton suivant pour pouvoir changer de question
            if (suivant) suivant.classList.add("show");
        });
    }
}
let numéroQuestion = 1;
let ScoreRéalisé = 0;
let counter;
let counterLine;
let widthValue = 0;
//L'addEventListener pour cliquer sur le bouton suivant pour passer à la question suivante ou au tableau de résultat pour le dernière question
suivant?.addEventListener("click", ()=>{
    if (index < questions.length - 1) {
        index++;
        numéroQuestion++;
        AfficherLaQuestion(index);
        AvancéeDesQuestions(numéroQuestion);
        clearInterval(counter);
        clearInterval(counterLine);
        LancerChrono(timeValue);
        Evolutionchrono(widthValue);
        if (timeText) timeText.textContent = "Chrono";
        suivant.classList.remove("show");
        if (evoChrono && timeCount && CaseChrono) {
            evoChrono.style.backgroundColor = "#007bff";
            timeCount.style.backgroundColor = "#0b4681";
            timeCount.style.borderColor = "#0b4681";
            CaseChrono.style.backgroundColor = "#cce5ff";
            CaseChrono.style.color = "#004085";
            CaseChrono.style.borderColor = "#b8daff";
        }
    } else {
        clearInterval(counter);
        clearInterval(counterLine);
        AfficherResultat();
    }
});
/**
 * Fonction pour afficher l'avancée du quizz, pour indiquer à quelle question on se trouve.
 * @param numéroQuestion le numéro de la question affichée*/ function AvancéeDesQuestions(numéroQuestion) {
    let totalQueCounTag = "<span><p>Question " + numéroQuestion + "</p> / <p>" + questions.length + "</p></span>";
    if (compteurAvancéQuestion) compteurAvancéQuestion.innerHTML = totalQueCounTag;
}
let timeValue = 15;
/** 
 * Fonction pour déclencher le chrono
 * @param timeValue le temps affiché au départ du chrono */ function LancerChrono(timeValue) {
    counter = setInterval(chrono, 1000);
    /** 
            * Fonction pour afficher le temps dans la partie chrono lors de l'affichage de la question */ function chrono() {
        if (timeCount) {
            timeCount.textContent = String(timeValue);
            timeValue--;
            if (timeValue < 9) {
                let addZero = timeCount.textContent;
                timeCount.textContent = "0" + addZero;
            }
            if (timeValue < 0) {
                clearInterval(counter);
                if (timeText) timeText.textContent = "Termin\xe9";
                //Afficher qu'elle était la bonne réponse si jamais le temps est écoulé
                const ToutesOptions = listeChoix?.children.length;
                let correcAns = questions[index].answer;
                if (ToutesOptions) {
                    let i;
                    for(i = 0; i < ToutesOptions; i++)if (listeChoix?.children[i].textContent == correcAns) {
                        listeChoix?.children[i].setAttribute("class", "option correct");
                        console.log("Temps \xe9coul\xe9, voici la bonne r\xe9ponse.");
                    }
                    //Enlever la possibilité de pouvoir répondre
                    for(i = 0; i < ToutesOptions; i++)listeChoix?.children[i].classList.add("disabled");
                    //Afficher le bouton suivant
                    suivant?.classList.add("show");
                }
            }
        }
    }
}
/**
 * Fonction pour afficher le container contenant le résultat*/ function AfficherResultat() {
    ContainerQuizz?.classList.add("cach\xe9");
    ContainerResultat?.classList.remove("cach\xe9");
    // Afficher un message différent en fonction du score
    if (ScoreRéalisé > 9) {
        let scoreTag = "<p>F\xe9licitatioooons ! Tu es imbattable, tu as eu <span> " + ScoreRéalisé + "</span> sur <span> " + questions.length + "</span>!</p>";
        if (MessageResultat) MessageResultat.innerHTML = scoreTag;
        if (imageResultat) imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/26u4exk4zsAqPcq08" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
    }
    if (ScoreRéalisé >= 7 && ScoreRéalisé <= 9) {
        let scoreTag1 = "<p>Bravooo ! Tu t'approches du sans faute, tu as eu <span>" + ScoreRéalisé + "</span> points sur <span>" + questions.length + "</span>.</p>";
        if (MessageResultat) MessageResultat.innerHTML = scoreTag1;
        if (imageResultat) imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/3oz8xRF0v9WMAUVLNK" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
    }
    if (ScoreRéalisé >= 5 && ScoreRéalisé < 7) {
        let scoreTag2 = "<p>C'est bien ! Tu sauves l'honneur, tu as eu <span>" + ScoreRéalisé + "</span> points sur <span>" + questions.length + "</span>.</p>";
        if (MessageResultat) MessageResultat.innerHTML = scoreTag2;
        if (imageResultat) imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/XreQmk7ETCak0" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
    }
    if (ScoreRéalisé >= 2 && ScoreRéalisé < 5) {
        let scoreTag3 = "<p>Aie ! C'est compliqu\xe9 l\xe0, tu as eu seulement <span>" + ScoreRéalisé + "</span> points sur <span>" + questions.length + "</span>.</p>";
        if (MessageResultat) MessageResultat.innerHTML = scoreTag3;
        if (imageResultat) imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/APqEbxBsVlkWSuFpth" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
    }
    if (ScoreRéalisé < 2) {
        let scoreTag4 = "<p>Oh non!! Tu as eu <span>" + ScoreRéalisé + "</span> point sur <span>" + questions.length + "</span>... Mais c'est certainement parce que tu es un g\xe9nie incompris!</p>";
        if (MessageResultat) MessageResultat.innerHTML = scoreTag4;
        if (imageResultat) imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/kv5d1BI0ZQbtV005Mv" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
    }
}
/**
 * Fonction pour représenter le temps restant avec une barre de progression
 * @param time la représentation du temps qui s'écoule sur une barre de progression donc au départ commence à 0 */ function Evolutionchrono(time) {
    counterLine = setInterval(chrono, 160);
    function chrono() {
        time += 1;
        if (evoChrono) evoChrono.style.width = time + "%";
        if (time > 50) {
            if (evoChrono && timeCount && CaseChrono) {
                evoChrono.style.backgroundColor = "orange";
                timeCount.style.backgroundColor = "#ce19348c";
                timeCount.style.borderColor = "#ce19348c";
                CaseChrono.style.backgroundColor = "orange";
                CaseChrono.style.color = "white";
                CaseChrono.style.borderColor = "orange";
            }
        }
        if (time > 80) {
            if (evoChrono && timeCount && CaseChrono) {
                evoChrono.style.backgroundColor = "red";
                timeCount.style.backgroundColor = "#ce19348c";
                timeCount.style.borderColor = "#ce19348c";
                CaseChrono.style.backgroundColor = "red";
                CaseChrono.style.color = "white";
                CaseChrono.style.borderColor = "red";
            }
        }
        if (time > 99) {
            clearInterval(counterLine);
            if (evoChrono && timeCount && CaseChrono) {
                evoChrono.style.backgroundColor = "#007bff";
                timeCount.style.backgroundColor = "#0b4681";
                timeCount.style.borderColor = "#0b4681";
                CaseChrono.style.backgroundColor = "#cce5ff";
                CaseChrono.style.color = "#004085";
                CaseChrono.style.borderColor = "#b8daff";
            }
        }
    }
}
//L'addEventListener si on clique sur le bouton "Refaire"
const rejouer = ContainerResultat?.querySelector(".buttons .continuer");
rejouer?.addEventListener("click", ()=>{
    ContainerQuizz?.classList.remove("cach\xe9");
    ContainerResultat?.classList.add("cach\xe9");
    timeValue = 15;
    index = 0;
    numéroQuestion = 1;
    ScoreRéalisé = 0;
    widthValue = 0;
    AfficherLaQuestion(index);
    AvancéeDesQuestions(numéroQuestion);
    clearInterval(counter);
    clearInterval(counterLine);
    LancerChrono(timeValue);
    Evolutionchrono(widthValue);
    if (timeText) timeText.textContent = "Temps";
    suivant?.classList.remove("show");
});
//L'addEventListener si on clique sur le bouton "Autre thème"
const stop = ContainerResultat?.querySelector(".buttons .arret");
stop?.addEventListener("click", ()=>{
    window.location.reload();
});
// Les tableaux d'objets des différents thèmes avec leurs questions, leurs images et leurs réponses 
let questions = [
    {
        image: '<img src="https://www.boutiquesdemusees.fr/uploads/photos/474/11220_xl.jpg" alt="La Joconde de L\xe9onar de Vinci" >',
        numb: 1,
        question: "Qui a fut accus\xe9 de complicit\xe9 lors du vol de La Joconde en 1911?",
        answer: "Pablo Picasso",
        options: [
            "Henri Matisse",
            "Ren\xe9 Magritte",
            "Pablo Picasso",
            "Salvador Dali"
        ]
    },
    {
        image: '<img src="https://www.alhuilesurtoile.com/im/articles/Warhol_MarilynMonroe_x9.jpg" alt="Portrait de Marilyn Monroe" >',
        numb: 2,
        question: "Qui a peint ces portraits de Marilyn Monroe?",
        answer: "Andy Warhol",
        options: [
            "Keith Haring",
            "Roy lichtenstein",
            "Jasper Jones",
            "Andy Warhol"
        ]
    },
    {
        image: '<img src="https://i0.wp.com/art-icle.fr/wp-content/uploads/2019/09/Giorgio-De-Chirico-copie.jpg?w=643&ssl=1" alt="Portrait de Guillaume Appolinaire" >',
        numb: 3,
        question: "Qui a peint ce portrait de Guillaume Apollinaire en 1914?",
        answer: "Giorgio De Chirico",
        options: [
            "Giorgio De Chirico",
            "Salvador Dali",
            "Pablo Picasso",
            "Auguste Renoir"
        ]
    },
    {
        image: '<img src="https://dailygeekshow.com/wp-content/uploads/2016/05/le-radeau-de-la-meduse.jpg" alt="Le Radeau de la M\xe9duse de Th\xe9odore G\xe9ricault">',
        numb: 4,
        question: "Pourquoi le tableau du Radeau de la M\xe9duse de Th\xe9odore G\xe9ricault est-il amen\xe9 \xe0 disparaitre?",
        answer: "Du plomb dans sa peinture l'oxyde.",
        options: [
            "C'est la volont\xe9 de l'artiste.",
            "Un champignon fragilise sa toile.",
            "Il a \xe9t\xe9 jug\xe9 choquant.",
            "Du plomb dans sa peinture l'oxyde."
        ]
    },
    {
        image: '<img src="https://media.vogue.fr/photos/5c2f50d37cec7f64be7c81c8/master/w_1200,h_1600,c_limit/2017_04_06_photo_00000071_jpg_8985_jpeg_2072.jpeg" alt="Treasures from the Wreck of the Unbelievable de Damien Hirst">',
        numb: 5,
        question: "En quelle ann\xe9e cette oeuvre en or a-t-elle \xe9t\xe9 r\xe9alis\xe9e?",
        answer: "En 2017 par Damien Hirst.",
        options: [
            "En 2017 par Damien Hirst.",
            "En 1520 avant J-C, lors de la victoire de Cortes sur le peuple Azt\xe8que.",
            "En 856, lors de la Festa della Sensa \xe0 Venise.",
            "En 1923 par Constantin Brancusi."
        ]
    },
    {
        image: '<img src="https://www.cobosocial.com/wp-content/uploads/2020/09/ARTS023146_press_resized.jpg" alt="Dessin dans le m\xe9tro de  New York">',
        numb: 6,
        question: "Qui a commenc\xe9 sa carri\xe8re en dessinant sur les murs du metro de New York?",
        answer: "Keith Haring",
        options: [
            "Banksy",
            "Mr Brainwash",
            "Keith Haring",
            "Speedy Graphito"
        ]
    },
    {
        image: '<img src="https://media.ouest-france.fr/v1/pictures/MjAxNjAyNTQ5YTUyOWNlOGZiMjM1YTYxM2ZlMzk0OTQ1MDFjMWI?width=640&height=480&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=d6d95a3633a618d8bd66405a2af8644757d3e3cae8e71b5e2c053d1993700965" alt="Restauration du Christ de Borja">',
        numb: 7,
        question: "Quelle oeuvre est devenue c\xe9l\xe8bre suite \xe0 une tentative de restauration compl\xe9tement rat\xe9e?",
        answer: "Le Christ de Borja",
        options: [
            "Le Christ de d'Arezzo",
            "Le Christ de Morella",
            "Le Christ de San Jos\xe9",
            "Le Christ de Borja"
        ]
    },
    {
        image: '<img src="https://artincontext.org/wp-content/uploads/2022/04/Sfumato-Examples.jpg" alt="Tableau de Gorgione">',
        numb: 8,
        question: "Quelle technique cherchant \xe0 cr\xe9er une impr\xe9cision des contours, fut popularis\xe9e en Italie du Nord au 15e si\xe8cle?",
        answer: "Le sfumato",
        options: [
            "Le colorito",
            "Le disegno",
            "Le sfumato",
            "La sprezzatura"
        ]
    },
    {
        image: '<img src="https://us.123rf.com/450wm/tenedos/tenedos1806/tenedos180600018/104328683-lintong-xi-an-shaanxi-chine-15-octobre-2014-les-c%C3%A9l%C3%A8bres-guerriers-en-terre-cuite-de-chine-l-arm%C3%A9e-d.jpg?ver=6" alt="Les guerriers en terre cuite de Shaanxi">',
        numb: 9,
        question: "Lors de fouilles arch\xe9ologiques \xe0 Shaanxi en Chine, combien a-t-on d\xe9couvert de statues de guerriers et de chevaux en terre cuite?",
        answer: "8000",
        options: [
            "6000",
            "8000",
            "2000",
            "4000"
        ]
    },
    {
        image: '<img src="https://www.passion-estampes.com/npe/fille-ruban.jpg" alt="Peinture de Roy Lichtenstein">',
        numb: 10,
        question: "\xc0 quel mouvement artistique appartiennent les oeuvres de Roy Lichtenstein?",
        answer: "Pop Art",
        options: [
            "Pop Art",
            "Surr\xe9alisme",
            "L'expressionnisme",
            "Le courant r\xe9aliste am\xe9ricain"
        ]
    }
];
let questionsarchi = [
    {
        image: '<img src="https://www.connaissancedesarts.com/wp-content/thumbnails/uploads/2020/04/cda19_article_actu-frank-lloyd-wright-recit-d-une-vie-tt-width-1200-height-630-fill-0-crop-1-bgcolor-ffffff.jpg" alt="Photographie de la demeure FallinWater" >',
        numb: 1,
        question: "Qui a con\xe7u la c\xe9l\xe8bre maison sur la cascade : FallingWater?",
        answer: "Frank Lloyd Wright",
        options: [
            "Frank Lloyd Wright",
            "Oscar Niemeyer",
            "Frank Gehry",
            "Jean Nouvel"
        ]
    },
    {
        image: '<img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/\xc9tienne-Louis_Boull\xe9e%2C_C\xe9notaphe_de_Newton_-_02_-_\xc9l\xe9vation_perspective.jpg" alt="C\xe9notaphe d\'Etienne-Louis Boul\xe9e" >',
        numb: 2,
        question: "Pourquoi parle-t-on d'architectures de papier pour Etienne-Louis Boull\xe9e?",
        answer: "Il a peu construit, son oeuvre est surtout dessin\xe9e.",
        options: [
            "Il revendiquait l'utilisation du papier dans ses mat\xe9riaux de construction.",
            "Il a construit plusieurs villes miniatures en papier.",
            "Il faisait partit d'un mouvement initi\xe9 par son ami Eug\xe8ne Papier.",
            "Il a peu construit, son oeuvre est surtout dessin\xe9e."
        ]
    },
    {
        image: '<img src="https://cdn.futura-sciences.com/sources/images/pyramide-Egypte.jpg" alt="Pyramides de Gizeh" >',
        numb: 3,
        question: "Qui a ordonn\xe9 la construction des pyramides de Gizeh?",
        answer: "Kh\xe9ops, Kh\xe9phren et Myk\xe9rinos",
        options: [
            "Ramses II, Kh\xe9ops et Cl\xe9opatre",
            "Thoutm\xf4sis III, Ramses II et Myk\xe9rinos",
            "Kh\xe9ops, Kh\xe9phren et Myk\xe9rinos",
            "Akhenaton, Tout\xe2nkhamon et Thoutm\xf4sis III"
        ]
    },
    {
        image: '<img src="https://images.adsttc.com/media/images/5f70/d528/63c0/17c2/6200/027c/medium_jpg/006.jpg?1601230107" alt="Biblioth\xe8que de Dujiangyan">',
        numb: 4,
        question: "O\xf9 se trouve cette biblioth\xe8que con\xe7ue par l'agence X + Living?",
        answer: "En Chine",
        options: [
            "En Allemagne",
            "En Angleterre",
            "En Chine",
            "Aux Etats-Unis"
        ]
    },
    {
        image: '<img src="https://www.lyoncapitale.fr/wp-content/uploads/2019/11/Exterior-shot-c-Eric-Cuvillier-770x433-770x433.jpg" alt="Photoraphie de l\'h\xf4tel Dieu \xe0 Lyon">',
        numb: 5,
        question: "Quel est le style de l'h\xf4tel Dieu de Lyon construit par Jacques-Germain Soufflot?",
        answer: "N\xe9o-classique",
        options: [
            "N\xe9o-classique",
            "Victorien",
            "Classique",
            "Moderne"
        ]
    },
    {
        image: '<img src="https://resize-parismatch.lanmedia.fr/var/pm/public/media/image/2022/03/16/19/En-Turquie-la-folie-des-grandeurs.jpg?VersionId=p6IhkmlcWEquCr3cRHsIU674CBy5.aTB" alt="Photoraphie de la ville abandonn\xe9e de Mudurnu">',
        numb: 6,
        question: "O\xf9 se trouve cette ville fant\xf4me de villas-ch\xe2teaux?",
        answer: "\xc0 Mudurnu, en Turquie",
        options: [
            "\xc0 Ptuj, en Slov\xe9nie",
            "\xc0 Mudurnu, en Turquie",
            "\xc0 Gdańsk, en Pologne",
            "\xc0 Saint-Gall, en Suisse"
        ]
    },
    {
        image: '<img src="https://www.impressions2voyage.net/wp-content/uploads/2018/05/Santa-Maria-Novella-Florence.jpg" alt="Photoraphie de Santa Maria Novella">',
        numb: 7,
        question: "En 1470, qui a compl\xe8t\xe9 la fa\xe7ade de l'\xe9glise Santa Maria Novella de Florence?",
        answer: "Leon Battista Alberti",
        options: [
            "Filippo Brunelleschi",
            "Michel-Ange",
            "Andrea Palladio",
            "Leon Battista Alberti"
        ]
    },
    {
        image: '<img src="https://www.inspirationde.com/wp-content/uploads/2014/06/inntel-hotels-amsterdam-zaandam-the-netherlands-flickr-photo-sharing-1403533312n84kg.jpg" alt="Photoraphie de l\'h\xf4tel Inntel Zaandam">',
        numb: 8,
        question: "O\xf9 se trouve l'h\xf4tel Inntel Zaandam con\xe7u par le studio WAM Architecten?",
        answer: "Amsterdam",
        options: [
            "Berlin",
            "Amsterdam",
            "Stockholm",
            "Oxford"
        ]
    },
    {
        image: '<img src="https://www.1jour1actu.com/wp-content/uploads/2021/06/VIDEO_jardin_francaise.jpg" alt="Photoraphie des jardins de Versailles">',
        numb: 9,
        question: "Comment s'appelait l'architecte paysagiste du ch\xe2teau de Versailles, qui a popularis\xe9 le style \"jardin \xe0 la fran\xe7aise\"?",
        answer: "Andr\xe9 Le N\xf4tre",
        options: [
            "Andr\xe9 Le N\xf4tre",
            "Jules Hardouin-Mansart",
            "Charles Le Brun",
            "Henri Duch\xeane"
        ]
    },
    {
        image: '<img src="https://i.pinimg.com/originals/9d/30/85/9d30856798cdbbf0ce5900a187f99131.jpg" alt="Photoraphie de colonnes">',
        numb: 10,
        question: "\xc0 quel style de colonne peut-on rattacher ce chapiteau?",
        answer: "Corinthien",
        options: [
            "Ionique",
            "Dorique",
            "Corinthien",
            "Byzantin"
        ]
    }
];
let questionsjv = [
    {
        image: '<img src="https://cdn.gamekult.com/images/gallery/33/339497/gris-pc-switch-6ec59fb5.jpg" alt="Screenshot du jeu Gris">',
        numb: 1,
        question: "Quel est le nom de ce jeu vid\xe9o qui permet d'explorer un paysage d'aquarelle?",
        answer: "Gris",
        options: [
            "Ori",
            "The longing",
            "Hob",
            "Gris"
        ]
    },
    {
        image: '<img src="https://static.hitek.fr/img/actualite/ill_m/1847060852/ninokuni1280x640.jpg" alt="screenshot de Nino Kuni" >',
        numb: 2,
        question: "Quel jeu pr\xe9sente un monde et des personnages dessin\xe9s par les studios Ghibli?",
        answer: "Ni No Kuni: La Vengeance de la sorci\xe8re c\xe9leste",
        options: [
            "Ni No Kuni: La Vengeance de la sorci\xe8re c\xe9leste",
            "Tunic",
            "Child of Light",
            "Yonder: The Cloud Catcher Chronicles"
        ]
    },
    {
        image: '<img src="https://ih1.redbubble.net/image.578505770.5548/flat,750x,075,f-pad,750x1000,f8f8f8.jpg" alt="Couverture du jeu Ico" >',
        numb: 3,
        question: "De quel artiste surr\xe9aliste, Fumito Ueda s'est-il inspir\xe9 pour cr\xe9er la couverture d'un de ses jeux?",
        answer: "Giorgio De Chirico",
        options: [
            "Salvador Dal\xed",
            "Alberto Giacometti",
            "Giorgio De Chirico",
            "Man Ray"
        ]
    },
    {
        image: '<img src="https://64.media.tumblr.com/1da244475203482c79f6675668ed20ed/tumblr_o2v2mwiwvi1uxd9mdo1_500.png" alt="Nuit \xe9toil\xe9e sur le Pontar">',
        numb: 4,
        question: 'Dans quel jeu peut-on acheter, lors d\'une qu\xeate, un tableau intitul\xe9 la "Nuit \xe9toil\xe9e sur le Pontar" de Van Rogh?',
        answer: "The Witcher 3 : Wild Hunt",
        options: [
            "Assassin's Creed Unity",
            "Les Sims 3",
            "Elden Ring",
            "The Witcher 3 : Wild Hunt"
        ]
    },
    {
        image: '<img src="https://i0.wp.com/vectis.ca/wp-content/uploads/2019/11/15617227372_550f75b5c8_b.png?fit=1200%2C628" alt="Image d\'un jeu en noir et blanc avec des taches de noir.">',
        numb: 5,
        question: 'Dans quel jeu, doit-on user de billes de couleur pour faire appara\xeetre le d\xe9cor car nous sommes perdu dans le r\xeave d\'un roi ayant d\xe9cid\xe9 de "repeindre" son royaume en blanc?',
        answer: "The Unfinished Swan",
        options: [
            "Gris",
            "The Unfinished Swan",
            "Hollow Knight",
            "Journey"
        ]
    },
    {
        image: '<img src="https://images.ladepeche.fr/api/v1/images/view/5c3718e73e454672250f58f7/large/image.jpg" alt="Assasssin\'s Creed \xe0 Venise.">',
        numb: 6,
        question: "Dans quel jeu, peut-on explorer la ville de Venise et d\xe9couvrir des points de vue unique sur la splendeur de son architecture?",
        answer: "Assassin's Creed II",
        options: [
            "Assassin's Creed: Brotherhood",
            "Assassin's Creed II",
            "Assassin's Creed III",
            "Assassin's Creed Odyssey"
        ]
    },
    {
        image: '<img src="https://blog.alsoknownas-clothing.com/sites/default/files/pictures/image-blog-article-invader.jpg" alt="Oeuvre de mosaique inspir\xe9e du jeu Space Invaders.">',
        numb: 7,
        question: "Quel street artist r\xe9alise, depuis 1996, des oeuvres en mosa\xefque inspir\xe9es de jeux vid\xe9os?",
        answer: "Invader",
        options: [
            "Banksy",
            "Vhils",
            "Shepard Fairey",
            "Invader"
        ]
    },
    {
        image: '<img src="https://cdn.cloudflare.steamstatic.com/steam/apps/308040/ss_95e28d58c73d0e0c0c75f1f378df66bd48bf75bf.1920x1080.jpg?t=1662709491" alt="Jeu vid\xe9o Back to Bed">',
        numb: 8,
        question: 'De quel mouvement artistique s\'inspire le jeu "Back to Bed"?',
        answer: "Le surr\xe9alisme",
        options: [
            "Le surr\xe9alisme",
            "Le cubisme",
            "Le pop art",
            "Le minimalisme"
        ]
    },
    {
        image: '<img src="https://static1.colliderimages.com/wordpress/wp-content/uploads/2022/01/Music-within-The-Legend-of-Zelda-Series.jpg" alt="Link et de la musique">',
        numb: 9,
        question: "De nombreux instruments apparaissent tout au long des jeux de la l\xe9gende de Zelda, mais lequel reste le plus c\xe9l\xe8bre des instruments utilis\xe9 par Link lors de ses qu\xeates?",
        answer: "L'Ocarina du temps",
        options: [
            "La Fl\xfbte spirituelle",
            "Les Maracas d’Hestu",
            "L'Ocarina du temps",
            "La Harpe des \xe2ges"
        ]
    },
    {
        image: '<img src="https://149362454.v2.pressablecdn.com/previously/wp-content/uploads/2016/05/61b2a55b-b0e8-4725-a009-23b458660396.jpg" alt="Illustration de la divine com\xe9die de Dante">',
        numb: 10,
        question: "De quel c\xe9l\xe8bre illustrateur du 19e si\xe8cle, l'univers et l'ambiance artistique des jeux Dark Souls pourrait-elle s'inspirer?",
        answer: "Gustave Dor\xe9",
        options: [
            "\xc9douard Manet",
            "Auguste Renoir",
            "Gustave Dor\xe9",
            "Camille Pissarro"
        ]
    }
];
let questionscine = [
    {
        image: '<img src="https://i.pinimg.com/originals/9f/8e/e9/9f8ee96324504a9ff06d9e25d0d41121.jpg" alt="Image du tableau et du film La jeune fille \xe0 la perle" >',
        numb: 1,
        question: 'Quelle actrice a jou\xe9 dans le film "La jeune fille \xe0 la perle"?',
        answer: "Scarlett Johansson",
        options: [
            "Emma Watson",
            "Scarlett Johansson",
            "Katherine Heigl",
            "Cameron Diaz"
        ]
    },
    {
        image: '<img src="https://i.pinimg.com/originals/9d/07/c3/9d07c32483a8c38d47c6f1a106a3fda7.jpg" alt="Nicholas Cage en Joconde" >',
        numb: 2,
        question: "Quel est cet acteur de cin\xe9ma?",
        answer: "Nicolas Cage",
        options: [
            "Tom Holland",
            "Bruce Willis",
            "Willem Dafoe",
            "Nicolas Cage"
        ]
    },
    {
        image: '<img src="https://media.vogue.fr/photos/5eb28a21d2efb0a42c0b4c70/master/w_2437,h_1612,c_limit/010_A7A08A84_285.jpg" alt="Film l\'affaire Thomas Crown" >',
        numb: 3,
        question: "Dans quel film un milliardaire r\xe9ussit-il \xe0 voler une toile de Claude Monet au Metropolitan Museum de New York?",
        answer: "L'affaire Thomas Crown",
        options: [
            "Braquage \xe0 l'italienne",
            "L'affaire Thomas Crown",
            "Arsene Lupin",
            "Haute Voltige"
        ]
    },
    {
        image: '<img src="https://i.pinimg.com/originals/89/a5/c0/89a5c079efb7b0054c4382f4bb4f73d8.jpg" alt="Photographie du film Frida Kahlo">',
        numb: 4,
        question: "Quelle actrice a incarn\xe9 la peintre mexicaine Frida Kahlo dans un biopic r\xe9compens\xe9 par 2 oscars?",
        answer: "Salma Hayek",
        options: [
            "Salma Hayek",
            "Eva Longoria",
            "Penelope Cruz",
            "Eva Mendes"
        ]
    },
    {
        image: '<img src="https://img.lapresse.ca/924x615/201803/01/1515065.jpg" alt="La passion Van Gogh">',
        numb: 5,
        question: "Quel film d'animation bas\xe9 sur 120 toiles d'un c\xe9l\xe8bre artiste, est le tout premier \xe0 avoir \xe9t\xe9 enti\xe8rement peint \xe0 la main?",
        answer: "La Passion Van Gogh",
        options: [
            "Linnea dans le jardin de Monet",
            "Andr\xe9 Derain et la chaleur du sud",
            "La Passion Van Gogh",
            "Olympia et Edouard Manet"
        ]
    },
    {
        image: '<img src="https://is5-ssl.mzstatic.com/image/thumb/eRZUYI5_BTj-Lij4BXIkow/1200x675.jpg" alt="Ed Harris en train de peindre">',
        numb: 6,
        question: "Quel artiste a \xe9t\xe9 interpr\xe9t\xe9 par Ed Harris dans un film \xe9ponyme?",
        answer: "Jackson Pollock",
        options: [
            "Pierre Soulages",
            "Jackson Pollock",
            "Joan Mir\xf3",
            "Pablo Picasso"
        ]
    },
    {
        image: '<img src="https://img.20mn.fr/kBnkvjJCSqiUwlfaJx2ZjA/768x492_20mn-12400" alt="Interview d\'un street-artist">',
        numb: 7,
        question: "Quel documentaire sur un artiste s'est finalement transform\xe9 en film r\xe9alis\xe9 par l'artiste lui-m\xeame?",
        answer: "Faites le mur! de Banksy ",
        options: [
            "Marina Abramovic: The Artist is Present",
            "Le Journal d’Andy Warhol",
            "Donald Judd : Less is now",
            "Faites le mur! de Banksy "
        ]
    },
    {
        image: '<img src="https://m.media-amazon.com/images/M/MV5BMTA2NzAzMTE2MDReQTJeQWpwZ15BbWU4MDExMTkyMDIx._V1_.jpg" alt="Bruce Willis menac\xe9 par des revolvers">',
        numb: 8,
        question: "Dans quel film Bruce Willis interpr\xe8te-t-il un cambrioleur contraint de d\xe9rober trois oeuvres de L\xe9onard de Vinci?",
        answer: "Hudson Hawk",
        options: [
            "Pi\xe8ge de cristal",
            "Expandables: unit\xe9 sp\xe9ciale",
            "Hudson Hawk",
            "58 minutes pour vivre"
        ]
    },
    {
        image: '<img src="https://www.jesolojournal.com/wp-content/uploads/2022/08/JJ30_travel-2-1024x683.jpg" alt="La passion Van Gogh">',
        numb: 9,
        question: "Dans quel film de Woody Allen, Julia Roberts interpr\xe8te-t-elle une historienne de l'art passionn\xe9e du Tintoret?",
        answer: "Everyone Says I Love You",
        options: [
            "Match Point",
            "Hannah et ses sœurs",
            "Everyone Says I Love You",
            "Stardust Memories"
        ]
    },
    {
        image: '<img src="https://www.lambiek.net/artists/image/m/mezieres/mezieres_circlesofpower.jpg" alt="Planche de bande dessin\xe9e">',
        numb: 10,
        question: 'Avec quel dessinateur de bande d\xe9ssin\xe9e, Luc Besson a-t-il collabor\xe9 pour son film "Le cinqui\xe8me \xe9l\xe9ment"?',
        answer: "Jean-Claude M\xe9zi\xe8res",
        options: [
            "Jean-Claude M\xe9zi\xe8res",
            "R\xe9gis Loisel",
            "Andr\xe9 Franquin",
            "Mathieu Bablet"
        ]
    }
];

},{}]},["eEc30","gg0zR"], "gg0zR", "parcelRequire529b")

//# sourceMappingURL=index.54632f9a.js.map
