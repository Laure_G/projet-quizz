
//Les variables
const start_btn = document.querySelector<HTMLElement>(".start_btn");
const start = document.querySelector<HTMLButtonElement>(".start");
const consignes = document.querySelector<HTMLButtonElement>(".consignes");
const ListeConsignes:string [] = [ " Vous aurez un temps limité de <span>15 secondes</span> pour répondre à chaque question.", " Toute réponse sélectionnée est <span>définitive</span>, vous ne pourrez cliquer qu'une fois.", " Vous gagnerez <span>1 point </span>à chaque bonne réponse."]
const ListInfo = document.querySelector<HTMLElement>(".ListInfo");
const Infos = document.querySelector<HTMLElement>(".Infos");
const ContainerTheme = document.querySelector<HTMLElement>(".ContainerTheme");
const quitter = document.querySelector<HTMLElement>(".quitter");
const continuer = document.querySelector<HTMLElement>(".continuer");
const art = document.querySelector<HTMLButtonElement>(".art");
const archi = document.querySelector<HTMLButtonElement>(".architecture");
const jeux = document.querySelector<HTMLButtonElement>(".jeux");
const cine = document.querySelector<HTMLButtonElement>(".cine");
const ContainerQuizz = document.querySelector<HTMLElement>(".ContainerQuizz");
const QuizzTitre = document.querySelector<HTMLElement>(".QuizzTitre");
const listeChoix = document.querySelector<HTMLElement>(".listeChoix");
const TexteQuestion = document.querySelector<HTMLElement>(".TexteQuestion");
const suivant = document.querySelector<HTMLElement>(".suivant");
const ImageQuestion = document.querySelector<HTMLElement>(".ImageQuestion");
const evoChrono = document.querySelector<HTMLElement>("header .evoChrono");
const timeText = document.querySelector<HTMLElement>(".chrono .time_left_txt");
const timeCount = document.querySelector<HTMLElement>(".chrono .timer_sec");
const CaseChrono = document.querySelector<HTMLElement>(".chrono");
const compteurAvancéQuestion = document.querySelector<HTMLElement>("footer .TotalQuestion");
const ContainerResultat = document.querySelector<HTMLElement>(".ContainerResultat");
const MessageResultat = document.querySelector<HTMLElement>(".MessageResultat");
const imageResultat = document.querySelector<HTMLImageElement>(".imageResultat");



// L'addEventListener pour le lancement du Quizz avec l'affichage des thèmes.
start?.addEventListener('click', () => {
  if (start_btn && ContainerTheme){
    ContainerTheme.classList.remove("caché")
    start_btn.classList.add("caché")
  }
})

//Création d'une boucle avec l'intégration du tableau de consignes dans le HTML sous forme de liste ordonnée.
let element: string;
for (element of ListeConsignes) {
let li = document.createElement("li");
li.classList.add("info")
ListInfo?.appendChild(li);
li.innerHTML = element}

// L'addEventListener pour le lancement du Quizz avec affichage des consignes.
consignes?.addEventListener('click', () => {
  if (Infos && start_btn){
    Infos.classList.remove("caché")
    start_btn.classList.add("caché")
  }
})

// L'addEventListener pour quitter les consignes
quitter?.addEventListener('click', () => {
  if (Infos && start_btn){
    Infos.classList.add("caché");
    start_btn.classList.remove("caché")
    
  }
})

// L'addEventListener pour commencer le jeu 
continuer?.addEventListener('click', () => {
  if (Infos && ContainerTheme){
    Infos.classList.add("caché");
    ContainerTheme.classList.remove("caché")
  }
})

// L'addEventListener pour choisir le thème Art 
art?.addEventListener('click', () => {
  if (ContainerTheme && ContainerQuizz){
    ContainerTheme.classList.add("caché");
    ContainerQuizz.classList.remove("caché")
    AfficherLaQuestion(0); 
    AvancéeDesQuestions(1);
    LancerChrono(15);
    Evolutionchrono(0); 
    if(QuizzTitre) {
      QuizzTitre.innerHTML="Quizz Art";
    }
  }
});

// L'addEventListener pour choisir le thème Architecture
archi?.addEventListener('click', () => {
  if (ContainerTheme && ContainerQuizz){
    ContainerTheme.classList.add("caché");
    ContainerQuizz.classList.remove("caché")
    questions = questionsarchi
    AfficherLaQuestion(0); 
    AvancéeDesQuestions(1);
    LancerChrono(15);
    Evolutionchrono(0); 
    if(QuizzTitre) {
      QuizzTitre.innerHTML="Quizz Architecture";
    }
  } 
})

// L'addEventListener pour choisir le thème Jeux Vidéos
jeux?.addEventListener('click', () => {
  if (ContainerTheme && ContainerQuizz){
    ContainerTheme.classList.add("caché");
    ContainerQuizz.classList.remove("caché");
    questions = questionsjv;
      AfficherLaQuestion(0); 
      AvancéeDesQuestions(1);
      LancerChrono(15);
      Evolutionchrono(0);
      if(QuizzTitre) {
        QuizzTitre.innerHTML="Quizz Jeux Vidéos";
      } 
    }    
  })
  
// L'addEventListener pour choisir le thème Cinéma
cine?.addEventListener('click', () => {
    if (ContainerTheme && ContainerQuizz){
      ContainerTheme.classList.add("caché");
      ContainerQuizz.classList.remove("caché");
      questions = questionscine;
        AfficherLaQuestion(0); 
        AvancéeDesQuestions(1);
        LancerChrono(15);
        Evolutionchrono(0); 
        if(QuizzTitre) {
          QuizzTitre.innerHTML="Quizz Cinéma";
        }
      } 
    })
       
let index:number = 0;
    
/**
 * Fonction pour afficher les questions et leurs options de réponse dans laquelle on a intégré le addEventListener de chaque option et leurs conséquences si la réponse est juste ou non.
 * @param index l'index de la question que l'on souhaite afficher avec ses options de réponse*/
function AfficherLaQuestion(index:number){
      let imageAffiche = '<div class="image"><span>'+ questions[index].image +'</span></div>'
      let questionAffiche = '<span>'+ questions[index].numb + ". " + questions[index].question +'</span>';
      let optionsAffiche = '<div class="option"><span>'+ questions[index].options[0] +'</span></div>'
      + '<div class="option"><span>'+ questions[index].options[1] +'</span></div>'
      + '<div class="option"><span>'+ questions[index].options[2] +'</span></div>'
      + '<div class="option"><span>'+ questions[index].options[3] +'</span></div>';
      
      if(TexteQuestion && listeChoix && ImageQuestion){
        TexteQuestion.innerHTML = questionAffiche; 
        listeChoix.innerHTML = optionsAffiche; 
        ImageQuestion.innerHTML = imageAffiche; 
      }
      
      // L'addEventListener pour donner un évenement clicable sur chaque option avec des conséquences différentes selon si c'est correct ou non
      if (listeChoix){
        
        const options = listeChoix.querySelectorAll<HTMLElement>(".option");
        let i:number;
         //On boucle pour pouvoir créer l'évènement click sur chaque option
        for(const itemOption of options){
          itemOption.addEventListener('click', () => {
             //Arrêt du chrono
            clearInterval(counter); 
            if(timeText){
              timeText.textContent = "Terminé"; }
            clearInterval(counterLine); 
            if(evoChrono && timeCount && CaseChrono){
                evoChrono.style.backgroundColor ="#007bff"
                timeCount.style.backgroundColor ="#0b4681"
                timeCount.style.borderColor ="#0b4681"
                CaseChrono.style.backgroundColor ="#cce5ff"
                CaseChrono.style.color ="#004085"
                CaseChrono.style.borderColor ="#b8daff"
              }      
              //Si la réponse est correcte
              if (itemOption.textContent == questions[index].answer) {
                ScoreRéalisé += 1; 
                itemOption.classList.add("correct");
                console.log ("Bonne réponse, tu as " + ScoreRéalisé + " points");
              }
              //Si la réponse est fausse
              if (itemOption.textContent != questions[index].answer) {
                itemOption.classList.add("incorrect");
                console.log("Mauvaise réponse, dommage!");
                
                //Afficher qu'elle était la bonne réponse si jamais on se trompe
                for(i=0; i < options.length; i++){
                  if(listeChoix.children[i].textContent == questions[index].answer){ 
                    listeChoix.children[i].setAttribute("class", "option correct"); 
                    console.log("Voici ce qu'il fallait répondre: " + options[i].textContent);
                  }
                }
              }
              //Empêcher de cliquer sur une autre question une fois que l'on a cliquer sur sa réponse
              for(i=0; i < options.length; i++){
                if (listeChoix){
                  listeChoix.children[i].classList.add("disabled");
                }
              }
              //Ajouter le bouton suivant pour pouvoir changer de question
              if(suivant){
                suivant.classList.add("show"); 
              }
            })
          }
        }
     }
 
let numéroQuestion = 1;
let ScoreRéalisé= 0;  
let counter:number;
let counterLine:any;
let widthValue = 0;
      
//L'addEventListener pour cliquer sur le bouton suivant pour passer à la question suivante ou au tableau de résultat pour le dernière question
 suivant?.addEventListener('click', () => {
        if(index < questions.length - 1){ 
          index++; 
          numéroQuestion++; 
          AfficherLaQuestion(index);
          AvancéeDesQuestions(numéroQuestion); 
          clearInterval(counter); 
          clearInterval(counterLine); 
          LancerChrono(timeValue); 
          Evolutionchrono(widthValue); 
          if(timeText){
            timeText.textContent = "Chrono";} 
            suivant.classList.remove("show"); 
            if(evoChrono && timeCount && CaseChrono){
              evoChrono.style.backgroundColor ="#007bff"
              timeCount.style.backgroundColor ="#0b4681"
              timeCount.style.borderColor ="#0b4681"
              CaseChrono.style.backgroundColor ="#cce5ff"
              CaseChrono.style.color ="#004085"
              CaseChrono.style.borderColor ="#b8daff"
            }
          }else{
            clearInterval(counter); 
            clearInterval(counterLine); 
            AfficherResultat(); 
          }
        });      

/**
 * Fonction pour afficher l'avancée du quizz, pour indiquer à quelle question on se trouve.
 * @param numéroQuestion le numéro de la question affichée*/
function AvancéeDesQuestions(numéroQuestion:number){
          let totalQueCounTag = '<span><p>Question '+ numéroQuestion +'</p> / <p>'+ questions.length +'</p></span>';
          if(compteurAvancéQuestion){
            compteurAvancéQuestion.innerHTML = totalQueCounTag; } 
  }
  
let timeValue =  15;
/** 
 * Fonction pour déclencher le chrono
 * @param timeValue le temps affiché au départ du chrono */
function LancerChrono(timeValue:number){
            counter = setInterval(chrono, 1000);
            /** 
            * Fonction pour afficher le temps dans la partie chrono lors de l'affichage de la question */
            function chrono(){
              if(timeCount){
                timeCount.textContent = String(timeValue); 
                timeValue--; 
                if(timeValue < 9){ 
                  let addZero = timeCount.textContent; 
                  timeCount.textContent = "0" + addZero; 
                }
                if(timeValue < 0){ 
                  clearInterval(counter); 
                  if(timeText){
                    timeText.textContent = "Terminé"; }
                    //Afficher qu'elle était la bonne réponse si jamais le temps est écoulé
                    const ToutesOptions = listeChoix?.children.length; 
                    let correcAns = questions[index].answer; 
                    if (ToutesOptions){
                      let i:number
                      for(i=0; i < ToutesOptions; i++){
                        if(listeChoix?.children[i].textContent == correcAns){ 
                          listeChoix?.children[i].setAttribute("class", "option correct"); 
                          console.log("Temps écoulé, voici la bonne réponse.");
                        }
                      }
                      //Enlever la possibilité de pouvoir répondre
                      for(i=0; i < ToutesOptions; i++){
                        listeChoix?.children[i].classList.add("disabled"); 
                      }
                      //Afficher le bouton suivant
                      suivant?.classList.add("show");
                    }}
                  }}
      }
                
/**
 * Fonction pour afficher le container contenant le résultat*/
function AfficherResultat(){  
  ContainerQuizz?.classList.add("caché");
  ContainerResultat?.classList.remove("caché"); 
  // Afficher un message différent en fonction du score
  if (ScoreRéalisé > 9){ 
    let scoreTag = '<p>Félicitatioooons ! Tu es imbattable, tu as eu <span> '+ ScoreRéalisé +'</span> sur <span> '+ questions.length +'</span>!</p>';
    if (MessageResultat){
      MessageResultat.innerHTML = scoreTag; } 
    if (imageResultat){
      imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/26u4exk4zsAqPcq08" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'}
    }

  if(ScoreRéalisé >= 7 && ScoreRéalisé <= 9){ 
      let scoreTag = "<p>Bravooo ! Tu t'approches du sans faute, tu as eu <span>"+ ScoreRéalisé +"</span> points sur <span>"+ questions.length +"</span>.</p>";
    if (MessageResultat){
      MessageResultat.innerHTML = scoreTag;}
    if (imageResultat){
      imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/3oz8xRF0v9WMAUVLNK" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'}
    }

  if(ScoreRéalisé >=5 && ScoreRéalisé < 7){ 
      let scoreTag = "<p>C'est bien ! Tu sauves l'honneur, tu as eu <span>"+ ScoreRéalisé +"</span> points sur <span>"+ questions.length +"</span>.</p>";
    if (MessageResultat){
      MessageResultat.innerHTML = scoreTag;}
    if (imageResultat){
      imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/XreQmk7ETCak0" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'}
    }


  if(ScoreRéalisé >=2 && ScoreRéalisé < 5){ 
      let scoreTag = "<p>Aie ! C'est compliqué là, tu as eu seulement <span>"+ ScoreRéalisé +"</span> points sur <span>"+ questions.length +"</span>.</p>";
    if (MessageResultat){
      MessageResultat.innerHTML = scoreTag;}
    if (imageResultat){
      imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/APqEbxBsVlkWSuFpth" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'}
    }
    
  if (ScoreRéalisé < 2) { 
      let scoreTag = "<p>Oh non!! Tu as eu <span>"+ ScoreRéalisé +"</span> point sur <span>"+ questions.length +"</span>... Mais c'est certainement parce que tu es un génie incompris!</p>";
    if (MessageResultat){
      MessageResultat.innerHTML = scoreTag;}
      if (imageResultat){
        imageResultat.innerHTML = '<iframe src="https://giphy.com/embed/kv5d1BI0ZQbtV005Mv" width="100%" height="100%" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'}
    }
    }
                          
/**
 * Fonction pour représenter le temps restant avec une barre de progression
 * @param time la représentation du temps qui s'écoule sur une barre de progression donc au départ commence à 0 */
function Evolutionchrono(time:number){
  counterLine = setInterval(chrono, 160);
  function chrono(){
    time += 1; 
    if(evoChrono){
      evoChrono.style.width = time + "%";} 
      if(time > 50){ 
        if(evoChrono && timeCount && CaseChrono){
          evoChrono.style.backgroundColor ="orange"
          timeCount.style.backgroundColor ="#ce19348c"
          timeCount.style.borderColor ="#ce19348c"
          CaseChrono.style.backgroundColor ="orange"
          CaseChrono.style.color ="white"
          CaseChrono.style.borderColor ="orange"
          
        }}
        if(time > 80){ 
          if(evoChrono && timeCount && CaseChrono){
            evoChrono.style.backgroundColor ="red"
            timeCount.style.backgroundColor ="#ce19348c"
            timeCount.style.borderColor ="#ce19348c"
            CaseChrono.style.backgroundColor ="red"
            CaseChrono.style.color ="white"
            CaseChrono.style.borderColor ="red"
          }
        }
        if(time > 99){ 
          clearInterval(counterLine); 
          if(evoChrono && timeCount && CaseChrono){
            evoChrono.style.backgroundColor ="#007bff"
            timeCount.style.backgroundColor ="#0b4681"
            timeCount.style.borderColor ="#0b4681"
            CaseChrono.style.backgroundColor ="#cce5ff"
            CaseChrono.style.color ="#004085"
            CaseChrono.style.borderColor ="#b8daff"
          }
        }
      }
    }
                              
//L'addEventListener si on clique sur le bouton "Refaire"
const rejouer = ContainerResultat?.querySelector<HTMLButtonElement>(".buttons .continuer");
rejouer?.addEventListener('click', () => {
  ContainerQuizz?.classList.remove("caché");
  ContainerResultat?.classList.add("caché");
  timeValue = 15; 
  index = 0;
  numéroQuestion = 1;
  ScoreRéalisé= 0;
  widthValue = 0;
  AfficherLaQuestion(index); 
  AvancéeDesQuestions(numéroQuestion); 
  clearInterval(counter); 
  clearInterval(counterLine); 
  LancerChrono(timeValue); 
  Evolutionchrono(widthValue); 
  if(timeText){
    timeText.textContent = "Temps"; }
    suivant?.classList.remove("show"); 
  })
                                
//L'addEventListener si on clique sur le bouton "Autre thème"
const stop = ContainerResultat?.querySelector<HTMLButtonElement>(".buttons .arret");
stop?.addEventListener('click', () => {
  window.location.reload();
})
                                
// Les tableaux d'objets des différents thèmes avec leurs questions, leurs images et leurs réponses 
let questions = [
    {
      image:'<img src="https://www.boutiquesdemusees.fr/uploads/photos/474/11220_xl.jpg" alt="La Joconde de Léonar de Vinci" >',
      numb: 1,
      question: "Qui a fut accusé de complicité lors du vol de La Joconde en 1911?",
      answer: "Pablo Picasso",
      options: [
        "Henri Matisse",
        "René Magritte",
        "Pablo Picasso",
        "Salvador Dali"
      ]
    },
    {
      image:'<img src="https://www.alhuilesurtoile.com/im/articles/Warhol_MarilynMonroe_x9.jpg" alt="Portrait de Marilyn Monroe" >',
      numb: 2,
      question: "Qui a peint ces portraits de Marilyn Monroe?",
      answer: "Andy Warhol",
      options: [
        "Keith Haring",
        "Roy lichtenstein",
        "Jasper Jones",
        "Andy Warhol"
      ]
    },
    {
      image:'<img src="https://i0.wp.com/art-icle.fr/wp-content/uploads/2019/09/Giorgio-De-Chirico-copie.jpg?w=643&ssl=1" alt="Portrait de Guillaume Appolinaire" >',
      numb: 3,
      question: "Qui a peint ce portrait de Guillaume Apollinaire en 1914?",
      answer: "Giorgio De Chirico",
      options: [
        "Giorgio De Chirico",
        "Salvador Dali",
        "Pablo Picasso",
        "Auguste Renoir"
      ]
    },
    {
      image:'<img src="https://dailygeekshow.com/wp-content/uploads/2016/05/le-radeau-de-la-meduse.jpg" alt="Le Radeau de la Méduse de Théodore Géricault">',
      numb: 4,
      question: "Pourquoi le tableau du Radeau de la Méduse de Théodore Géricault est-il amené à disparaitre?",
      answer: "Du plomb dans sa peinture l'oxyde.",
      options: [
        "C'est la volonté de l'artiste.",
        "Un champignon fragilise sa toile.",
        "Il a été jugé choquant.",
        "Du plomb dans sa peinture l'oxyde."
      ]
    },
    {
      image:'<img src="https://media.vogue.fr/photos/5c2f50d37cec7f64be7c81c8/master/w_1200,h_1600,c_limit/2017_04_06_photo_00000071_jpg_8985_jpeg_2072.jpeg" alt="Treasures from the Wreck of the Unbelievable de Damien Hirst">',
      numb: 5,
      question: "En quelle année cette oeuvre en or a-t-elle été réalisée?",
      answer: "En 2017 par Damien Hirst.",
      options: [
        "En 2017 par Damien Hirst.",
        "En 1520 avant J-C, lors de la victoire de Cortes sur le peuple Aztèque.",
        "En 856, lors de la Festa della Sensa à Venise.",
        "En 1923 par Constantin Brancusi."
      ]
    },
    {
      image:'<img src="https://www.gwarlingo.com/wp-content/uploads/2012/09/Haring-in-Subway.jpg" alt="Dessin dans le métro de  New York">',
      numb: 6,
      question: "Qui a commencé sa carrière en dessinant sur les murs du metro de New York?",
      answer: "Keith Haring",
      options: [
        "Banksy",
        "Mr Brainwash",
        "Keith Haring",
        "Speedy Graphito"
      ]
    },
    {
      image:'<img src="https://media.ouest-france.fr/v1/pictures/MjAxNjAyNTQ5YTUyOWNlOGZiMjM1YTYxM2ZlMzk0OTQ1MDFjMWI?width=640&height=480&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=d6d95a3633a618d8bd66405a2af8644757d3e3cae8e71b5e2c053d1993700965" alt="Restauration du Christ de Borja">',
      numb: 7,
      question: "Quelle oeuvre est devenue célèbre suite à une tentative de restauration complétement ratée?",
      answer: "Le Christ de Borja",
      options: [
        "Le Christ de d'Arezzo",
        "Le Christ de Morella",
        "Le Christ de San José",
        "Le Christ de Borja"
      ]
    },
    {
      image:'<img src="https://artincontext.org/wp-content/uploads/2022/04/Sfumato-Examples.jpg" alt="Tableau de Gorgione">',
      numb: 8,
      question: "Quelle technique cherchant à créer une imprécision des contours, fut popularisée en Italie du Nord au 15e siècle?",
      answer: "Le sfumato",
      options: [
        "Le colorito",
        "Le disegno",
        "Le sfumato",
        "La sprezzatura"
      ]
    }
    ,
    {
      image:'<img src="https://us.123rf.com/450wm/tenedos/tenedos1806/tenedos180600018/104328683-lintong-xi-an-shaanxi-chine-15-octobre-2014-les-c%C3%A9l%C3%A8bres-guerriers-en-terre-cuite-de-chine-l-arm%C3%A9e-d.jpg?ver=6" alt="Les guerriers en terre cuite de Shaanxi">',
      numb: 9,
      question: "Lors de fouilles archéologiques à Shaanxi en Chine, combien a-t-on découvert de statues de guerriers et de chevaux en terre cuite?",
      answer: "8000",
      options: [
        "6000",
        "8000",
        "2000",
        "4000"
      ]
    },
    {
      image:'<img src="https://www.passion-estampes.com/npe/fille-ruban.jpg" alt="Peinture de Roy Lichtenstein">',
      numb: 10,
      question: "À quel mouvement artistique appartiennent les oeuvres de Roy Lichtenstein?",
      answer: "Pop Art",
      options: [
        "Pop Art",
        "Surréalisme",
        "L'expressionnisme",
        "Le courant réaliste américain"
      ]
    },
]

let questionsarchi = [
  {
    image:'<img src="https://www.connaissancedesarts.com/wp-content/thumbnails/uploads/2020/04/cda19_article_actu-frank-lloyd-wright-recit-d-une-vie-tt-width-1200-height-630-fill-0-crop-1-bgcolor-ffffff.jpg" alt="Photographie de la demeure FallinWater" >',
    numb: 1,
    question: "Qui a conçu la célèbre maison sur la cascade : FallingWater?",
    answer: "Frank Lloyd Wright",
    options: [
      "Frank Lloyd Wright",
      "Oscar Niemeyer",
      "Frank Gehry",
      "Jean Nouvel"
    ]
  },
  {
    image:'<img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/Étienne-Louis_Boullée%2C_Cénotaphe_de_Newton_-_02_-_Élévation_perspective.jpg" alt="Cénotaphe d\'Etienne-Louis Boulée" >',
    numb: 2,
    question: "Pourquoi parle-t-on d'architectures de papier pour Etienne-Louis Boullée?",
    answer: "Il a peu construit, son oeuvre est surtout dessinée.",
    options: [
      "Il revendiquait l'utilisation du papier dans ses matériaux de construction.",
      "Il a construit plusieurs villes miniatures en papier.",
      "Il faisait partit d'un mouvement initié par son ami Eugène Papier.",
      "Il a peu construit, son oeuvre est surtout dessinée."
    ]
  },
  {
    image:'<img src="https://cdn.futura-sciences.com/sources/images/pyramide-Egypte.jpg" alt="Pyramides de Gizeh" >',
    numb: 3,
    question: "Qui a ordonné la construction des pyramides de Gizeh?",
    answer: "Khéops, Khéphren et Mykérinos",
    options: [
      "Ramses II, Khéops et Cléopatre",
      "Thoutmôsis III, Ramses II et Mykérinos",
      "Khéops, Khéphren et Mykérinos",
      "Akhenaton, Toutânkhamon et Thoutmôsis III"
    ]
  },
  {
    image:'<img src="https://images.adsttc.com/media/images/5f70/d528/63c0/17c2/6200/027c/medium_jpg/006.jpg?1601230107" alt="Bibliothèque de Dujiangyan">',
    numb: 4,
    question: "Où se trouve cette bibliothèque conçue par l'agence X + Living?",
    answer: "En Chine",
    options: [
      "En Allemagne",
      "En Angleterre",
      "En Chine",
      "Aux Etats-Unis"
    ]
  },{
    image:'<img src="https://www.lyoncapitale.fr/wp-content/uploads/2019/11/Exterior-shot-c-Eric-Cuvillier-770x433-770x433.jpg" alt="Photoraphie de l\'hôtel Dieu à Lyon">',
    numb: 5,
    question: "Quel est le style de l'hôtel Dieu de Lyon construit par Jacques-Germain Soufflot?",
    answer: "Néo-classique",
    options: [
      "Néo-classique",
      "Victorien",
      "Classique",
      "Moderne"
    ]
  },{
    image:'<img src="https://resize-parismatch.lanmedia.fr/var/pm/public/media/image/2022/03/16/19/En-Turquie-la-folie-des-grandeurs.jpg?VersionId=p6IhkmlcWEquCr3cRHsIU674CBy5.aTB" alt="Photoraphie de la ville abandonnée de Mudurnu">',
    numb: 6,
    question: "Où se trouve cette ville fantôme de villas-châteaux?",
    answer: "À Mudurnu, en Turquie",
    options: [
      "À Ptuj, en Slovénie",
      "À Mudurnu, en Turquie",
      "À Gdańsk, en Pologne",
      "À Saint-Gall, en Suisse"
    ]
  },{
    image:'<img src="https://www.impressions2voyage.net/wp-content/uploads/2018/05/Santa-Maria-Novella-Florence.jpg" alt="Photoraphie de Santa Maria Novella">',
    numb: 7,
    question: "En 1470, qui a complèté la façade de l'église Santa Maria Novella de Florence?",
    answer: "Leon Battista Alberti",
    options: [
      "Filippo Brunelleschi",
      "Michel-Ange",
      "Andrea Palladio",
      "Leon Battista Alberti"
    ]
  },{
    image:'<img src="https://www.inspirationde.com/wp-content/uploads/2014/06/inntel-hotels-amsterdam-zaandam-the-netherlands-flickr-photo-sharing-1403533312n84kg.jpg" alt="Photoraphie de l\'hôtel Inntel Zaandam">',
    numb: 8,
    question: "Où se trouve l\'hôtel Inntel Zaandam conçu par le studio WAM Architecten?",
    answer: "Amsterdam",
    options: [
      "Berlin",
      "Amsterdam",
      "Stockholm",
      "Oxford"
    ]
  },{
    image:'<img src="https://www.1jour1actu.com/wp-content/uploads/2021/06/VIDEO_jardin_francaise.jpg" alt="Photoraphie des jardins de Versailles">',
    numb: 9,
    question: "Comment s'appelait l'architecte paysagiste du château de Versailles, qui a popularisé le style \"jardin à la française\"?",
    answer: "André Le Nôtre",
    options: [
      "André Le Nôtre",
      "Jules Hardouin-Mansart",
      "Charles Le Brun",
      "Henri Duchêne"
    ]
  },{
    image:'<img src="https://i.pinimg.com/originals/9d/30/85/9d30856798cdbbf0ce5900a187f99131.jpg" alt="Photoraphie de colonnes">',
    numb: 10,
    question: "À quel style de colonne peut-on rattacher ce chapiteau?",
    answer: "Corinthien",
    options: [
      "Ionique",
      "Dorique",
      "Corinthien",
      "Byzantin"
    ]
  }
]

let questionsjv = [
  {
    image:'<img src="https://cdn.gamekult.com/images/gallery/33/339497/gris-pc-switch-6ec59fb5.jpg" alt="Screenshot du jeu Gris">',
    numb: 1,
    question: "Quel est le nom de ce jeu vidéo qui permet d'explorer un paysage d'aquarelle?",
    answer: "Gris",
    options: [
      "Ori",
      "The longing",
      "Hob",
      "Gris"
    ]
  },
  {
    image:'<img src="https://static.hitek.fr/img/actualite/ill_m/1847060852/ninokuni1280x640.jpg" alt="screenshot de Nino Kuni" >',
    numb: 2,
    question: "Quel jeu présente un monde et des personnages dessinés par les studios Ghibli?",
    answer: "Ni No Kuni: La Vengeance de la sorcière céleste",
    options: [
      "Ni No Kuni: La Vengeance de la sorcière céleste",
      "Tunic",
      "Child of Light",
      "Yonder: The Cloud Catcher Chronicles"
    ]
  },
  {
    image:'<img src="https://ih1.redbubble.net/image.578505770.5548/flat,750x,075,f-pad,750x1000,f8f8f8.jpg" alt="Couverture du jeu Ico" >',
    numb: 3,
    question: "De quel artiste surréaliste, Fumito Ueda s'est-il inspiré pour créer la couverture d'un de ses jeux?",
    answer: "Giorgio De Chirico",
    options: [
      "Salvador Dalí",
      "Alberto Giacometti",
      "Giorgio De Chirico",
      "Man Ray"
    ]
  },
  {
    image:'<img src="https://64.media.tumblr.com/1da244475203482c79f6675668ed20ed/tumblr_o2v2mwiwvi1uxd9mdo1_500.png" alt="Nuit étoilée sur le Pontar">',
    numb: 4,
    question: "Dans quel jeu peut-on acheter, lors d'une quête, un tableau intitulé la \"Nuit étoilée sur le Pontar\" de Van Rogh?",
    answer: "The Witcher 3 : Wild Hunt",
    options: [
      "Assassin's Creed Unity",
      "Les Sims 3",
      "Elden Ring",
      "The Witcher 3 : Wild Hunt"
    ]
  },
  {
    image:'<img src="https://i0.wp.com/vectis.ca/wp-content/uploads/2019/11/15617227372_550f75b5c8_b.png?fit=1200%2C628" alt="Image d\'un jeu en noir et blanc avec des taches de noir.">',
    numb: 5,
    question: "Dans quel jeu, doit-on user de billes de couleur pour faire apparaître le décor car nous sommes perdu dans le rêve d'un roi ayant décidé de \"repeindre\" son royaume en blanc?",
    answer: "The Unfinished Swan",
    options: [
      "Gris",
      "The Unfinished Swan",
      "Hollow Knight",
      "Journey"
    ]
  },
  {
    image:'<img src="https://images.ladepeche.fr/api/v1/images/view/5c3718e73e454672250f58f7/large/image.jpg" alt="Assasssin\'s Creed à Venise.">',
    numb: 6,
    question: "Dans quel jeu, peut-on explorer la ville de Venise et découvrir des points de vue unique sur la splendeur de son architecture?",
    answer: "Assassin's Creed II",
    options: [
      "Assassin\'s Creed: Brotherhood",
      "Assassin\'s Creed II",
      "Assassin\'s Creed III",
      "Assassin\'s Creed Odyssey"
    ]
  },
  {
    image:'<img src="https://blog.alsoknownas-clothing.com/sites/default/files/pictures/image-blog-article-invader.jpg" alt="Oeuvre de mosaique inspirée du jeu Space Invaders.">',
    numb: 7,
    question: "Quel street artist réalise, depuis 1996, des oeuvres en mosaïque inspirées de jeux vidéos?",
    answer: "Invader",
    options: [
      "Banksy",
      "Vhils",
      "Shepard Fairey",
      "Invader"
    ]
  },{
    image:'<img src="https://cdn.cloudflare.steamstatic.com/steam/apps/308040/ss_95e28d58c73d0e0c0c75f1f378df66bd48bf75bf.1920x1080.jpg?t=1662709491" alt="Jeu vidéo Back to Bed">',
    numb: 8,
    question: "De quel mouvement artistique s'inspire le jeu \"Back to Bed\"?",
    answer: "Le surréalisme",
    options: [
      "Le surréalisme",
      "Le cubisme",
      "Le pop art",
      "Le minimalisme"
    ]
  },{
    image:'<img src="https://static1.colliderimages.com/wordpress/wp-content/uploads/2022/01/Music-within-The-Legend-of-Zelda-Series.jpg" alt="Link et de la musique">',
    numb: 9,
    question: "De nombreux instruments apparaissent tout au long des jeux de la légende de Zelda, mais lequel reste le plus célèbre des instruments utilisé par Link lors de ses quêtes?",
    answer: "L'Ocarina du temps",
    options: [
      "La Flûte spirituelle",
      "Les Maracas d’Hestu",
      "L'Ocarina du temps",
      "La Harpe des âges"
    ]
  },{
    image:'<img src="https://149362454.v2.pressablecdn.com/previously/wp-content/uploads/2016/05/61b2a55b-b0e8-4725-a009-23b458660396.jpg" alt="Illustration de la divine comédie de Dante">',
    numb: 10,
    question: "De quel célèbre illustrateur du 19e siècle, l'univers et l'ambiance artistique des jeux Dark Souls pourrait-elle s'inspirer?",
    answer: "Gustave Doré",
    options: [
      "Édouard Manet",
      "Auguste Renoir",
      "Gustave Doré",
      "Camille Pissarro"
    ]
  },]

let questionscine =[
  {
    image:'<img src="https://i.pinimg.com/originals/9f/8e/e9/9f8ee96324504a9ff06d9e25d0d41121.jpg" alt="Image du tableau et du film La jeune fille à la perle" >',
    numb: 1,
    question: "Quelle actrice a joué dans le film \"La jeune fille à la perle\"?",
    answer: "Scarlett Johansson",
    options: [
      "Emma Watson",
      "Scarlett Johansson",
      "Katherine Heigl",
      "Cameron Diaz"
    ]
  },
  {
    image:'<img src="https://i.pinimg.com/originals/9d/07/c3/9d07c32483a8c38d47c6f1a106a3fda7.jpg" alt="Nicholas Cage en Joconde" >',
    numb: 2,
    question: "Quel est cet acteur de cinéma?",
    answer: "Nicolas Cage",
    options: [
      "Tom Holland",
      "Bruce Willis",
      "Willem Dafoe",
      "Nicolas Cage"
    ]
  },
  {
    image:'<img src="https://media.vogue.fr/photos/5eb28a21d2efb0a42c0b4c70/master/w_2437,h_1612,c_limit/010_A7A08A84_285.jpg" alt="Film l\'affaire Thomas Crown" >',
    numb: 3,
    question: "Dans quel film un milliardaire réussit-il à voler une toile de Claude Monet au Metropolitan Museum de New York?",
    answer: "L'affaire Thomas Crown",
    options: [
      "Braquage à l'italienne",
      "L'affaire Thomas Crown",
      "Arsene Lupin",
      "Haute Voltige"
    ]
  },
  {
    image:'<img src="https://i.pinimg.com/originals/89/a5/c0/89a5c079efb7b0054c4382f4bb4f73d8.jpg" alt="Photographie du film Frida Kahlo">',
    numb: 4,
    question: "Quelle actrice a incarné la peintre mexicaine Frida Kahlo dans un biopic récompensé par 2 oscars?",
    answer: "Salma Hayek",
    options: [
      "Salma Hayek",
      "Eva Longoria",
      "Penelope Cruz",
      "Eva Mendes"
    ]
  },
  {
    image:'<img src="https://img.lapresse.ca/924x615/201803/01/1515065.jpg" alt="La passion Van Gogh">',
    numb: 5,
    question: "Quel film d'animation basé sur 120 toiles d'un célèbre artiste, est le tout premier à avoir été entièrement peint à la main?",
    answer: "La Passion Van Gogh",
    options: [
      "Linnea dans le jardin de Monet",
      "André Derain et la chaleur du sud",
      "La Passion Van Gogh",
      "Olympia et Edouard Manet"
    ]
  },{
    image:'<img src="https://is5-ssl.mzstatic.com/image/thumb/eRZUYI5_BTj-Lij4BXIkow/1200x675.jpg" alt="Ed Harris en train de peindre">',
    numb: 6,
    question: "Quel artiste a été interprété par Ed Harris dans un film éponyme?",
    answer: "Jackson Pollock",
    options: [
      "Pierre Soulages",
      "Jackson Pollock",
      "Joan Miró",
      "Pablo Picasso"
    ]
  },
  {
    image:'<img src="https://img.20mn.fr/kBnkvjJCSqiUwlfaJx2ZjA/768x492_20mn-12400" alt="Interview d\'un street-artist">',
    numb: 7,
    question: "Quel documentaire sur un artiste s'est finalement transformé en film réalisé par l'artiste lui-même?",
    answer: "Faites le mur! de Banksy ",
    options: [
      "Marina Abramovic: The Artist is Present",
      "Le Journal d’Andy Warhol",
      "Donald Judd : Less is now",
      "Faites le mur! de Banksy "
    ]
  },{
    image:'<img src="https://m.media-amazon.com/images/M/MV5BMTA2NzAzMTE2MDReQTJeQWpwZ15BbWU4MDExMTkyMDIx._V1_.jpg" alt="Bruce Willis menacé par des revolvers">',
    numb: 8,
    question: "Dans quel film Bruce Willis interprète-t-il un cambrioleur contraint de dérober trois oeuvres de Léonard de Vinci?",
    answer: "Hudson Hawk",
    options: [
      "Piège de cristal",
      "Expandables: unité spéciale",
      "Hudson Hawk",
      "58 minutes pour vivre"
    ]
  },{
    image:'<img src="https://www.jesolojournal.com/wp-content/uploads/2022/08/JJ30_travel-2-1024x683.jpg" alt="La passion Van Gogh">',
    numb: 9,
    question: "Dans quel film de Woody Allen, Julia Roberts interprète-t-elle une historienne de l'art passionnée du Tintoret?",
    answer: "Everyone Says I Love You",
    options: [
      "Match Point",
      "Hannah et ses sœurs",
      "Everyone Says I Love You",
      "Stardust Memories"
    ]
  },{
    image:'<img src="https://www.lambiek.net/artists/image/m/mezieres/mezieres_circlesofpower.jpg" alt="Planche de bande dessinée">',
    numb: 10,
    question: "Avec quel dessinateur de bande déssinée, Luc Besson a-t-il collaboré pour son film \"Le cinquième élément\"?",
    answer: "Jean-Claude Mézières",
    options: [
      "Jean-Claude Mézières",
      "Régis Loisel",
      "André Franquin",
      "Mathieu Bablet"
    ]
  },]
                                
                                
                                